package com.gcloud.core.identity;

import java.io.Serializable;

import com.gcloud.header.identity.user.model.GetUserModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class TokenUser implements Serializable{
	private GetUserModel userInfo;
//	private String userId;
	private Long expressTime;
//	private String loginName;
	public GetUserModel getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(GetUserModel userInfo) {
		this.userInfo = userInfo;
	}
	public Long getExpressTime() {
		return expressTime;
	}
	public void setExpressTime(Long expressTime) {
		this.expressTime = expressTime;
	}
}