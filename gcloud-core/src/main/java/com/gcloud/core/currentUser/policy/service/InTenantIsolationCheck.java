package com.gcloud.core.currentUser.policy.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.gcloud.core.currentUser.policy.enums.ResourceIsolationType;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.common.ResourceOwner;
import com.gcloud.header.enums.ResourceRightType;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class InTenantIsolationCheck implements IIsolationTypeCheck{
	@PostConstruct
    private void init() {

    }
	
	@Override
	public ResourceIsolationType resourceIsolationType() {
		return ResourceIsolationType.IN_TENANT;
	}

	@Override
	public void check(ResourceOwner resourceOwner, CurrentUser currentUser) {
		//租户隔离
		if(StringUtils.isNotBlank(currentUser.getDefaultTenant())) {
			if(!currentUser.getDefaultTenant().equals(resourceOwner.getTenantId())) {
				throw new GCloudException("当前用户没有权限操作该资�?");
			}
		}else if(!currentUser.getUserTenants().contains(resourceOwner.getTenantId())) {
			throw new GCloudException("当前用户没有权限操作该资�?");
		}
	}

	@Override
	public FilterPolicyModel resourceFilter(CurrentUser currentUser, String prefix) {
		FilterPolicyModel model = new FilterPolicyModel();
		List<Object> params = new ArrayList<Object>();
		StringBuffer sqlWhereBuff = new StringBuffer();
		sqlWhereBuff.append(" AND ");
		if(currentUser.getUserTenants().size() > 0) {
			if(!StringUtils.isBlank(currentUser.getDefaultTenant())) {
					sqlWhereBuff.append( prefix + "tenant_id = ? AND ");
					params.add(currentUser.getDefaultTenant());
			}
			sqlWhereBuff.append( prefix + "tenant_id IN ( ");
			sqlWhereBuff.append(SqlUtil.inPreStr(currentUser.getUserTenants().size()));
			sqlWhereBuff.append(")");
			params.addAll(currentUser.getUserTenants());
			
		} else {
			sqlWhereBuff.append( "1 != 1 ");
		}
		model.setParams(params);
		model.setWhereSql(sqlWhereBuff.toString());
		
		return model;
	}

}