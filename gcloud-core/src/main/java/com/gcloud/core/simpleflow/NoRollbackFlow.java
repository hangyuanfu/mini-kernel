package com.gcloud.core.simpleflow;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public abstract class NoRollbackFlow<T> extends Flow<T>{
	public NoRollbackFlow(){
		
	}
	public NoRollbackFlow(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public void rollback(SimpleFlowChain chain,T data){
		chain.rollback();
	}
}