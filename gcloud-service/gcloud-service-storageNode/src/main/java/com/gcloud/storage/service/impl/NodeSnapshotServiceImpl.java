package com.gcloud.storage.service.impl;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.storage.driver.StorageDrivers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class NodeSnapshotServiceImpl {

    @Autowired
    private StorageDrivers drivers;

    @Autowired
    private StoragePools pools;

    public void createSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        drivers.get(driverName).createSnapshot(pools.checkAndGet(poolName), volumeRefId, snapshotId, snapshotRefId);
    }

    public void deleteSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException {
        drivers.get(driverName).deleteSnapshot(pools.checkAndGet(poolName), volumeRefId, snapshotId, snapshotRefId);
    }

    public List<String> resetSnapshot(String storageType, String poolName, String driverName, String volumeRefId, String snapshotId, String snapshotRefId, Integer size)
            throws GCloudException {
        return drivers.get(driverName).resetSnapshot(pools.checkAndGet(poolName), volumeRefId, snapshotId, snapshotRefId, size);
    }

}