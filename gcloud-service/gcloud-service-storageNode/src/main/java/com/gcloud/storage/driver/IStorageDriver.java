package com.gcloud.storage.driver;

import java.util.List;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.ResourceProviderVo;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.storage.NodeStoragePoolVo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IStorageDriver {

    StoragePoolDriver driver();

    void createDisk(NodeStoragePoolVo pool, String volumeId, Integer size, String imageId, String snapshotId) throws GCloudException;

    void deleteDisk(NodeStoragePoolVo pool, String volumeId, List<ResourceProviderVo> snapshots) throws GCloudException;

    void resizeDisk(NodeStoragePoolVo pool, String volumeId, Integer oldSize, Integer newSize) throws GCloudException;

    void createSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException;

    void deleteSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId, String snapshotRefId) throws GCloudException;

    List<String> resetSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId, String snapshotRefId, Integer size) throws GCloudException;

}