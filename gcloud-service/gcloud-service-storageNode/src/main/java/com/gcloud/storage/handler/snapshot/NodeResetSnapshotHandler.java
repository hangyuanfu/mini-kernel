package com.gcloud.storage.handler.snapshot;

import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.storage.msg.node.volume.NodeResetSnapshotMsg;
import com.gcloud.header.storage.msg.node.volume.NodeResetSnapshotReplyMsg;
import com.gcloud.storage.service.impl.NodeSnapshotServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class NodeResetSnapshotHandler extends AsyncMessageHandler<NodeResetSnapshotMsg> {

    @Autowired
    private NodeSnapshotServiceImpl snapshotService;

    @Autowired
    private MessageBus bus;

    @Override
    public void handle(NodeResetSnapshotMsg msg) {
        NodeResetSnapshotReplyMsg replyMsg = msg.deriveMsg(NodeResetSnapshotReplyMsg.class);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        replyMsg.setSnapshotId(msg.getSnapshotId());
        try {
            log.info("正在reset快照：{}, volume {}, snapshot {}", msg.getStorageType(), msg.getVolumeRefId(), msg.getSnapshotRefId());
            replyMsg.setSnapshotsToDelete(this.snapshotService.resetSnapshot(msg.getStorageType(), msg.getPoolName(), msg.getDriverName(), msg.getVolumeRefId(),
                    msg.getSnapshotId(), msg.getSnapshotRefId(), msg.getSize()));
            replyMsg.setSuccess(true);
            log.info("reset快照成功：{}, {}", msg.getStorageType(), msg.getSnapshotRefId());
        }
        catch (Exception ex) {
            log.error("::reset快照失败", ex);
            replyMsg.setSuccess(false);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::reset快照失败"));
        }
        this.bus.send(replyMsg);
    }

}