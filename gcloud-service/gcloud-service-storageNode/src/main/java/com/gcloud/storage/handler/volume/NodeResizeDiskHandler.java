package com.gcloud.storage.handler.volume;

import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.storage.msg.node.volume.NodeResizeDiskMsg;
import com.gcloud.header.storage.msg.node.volume.NodeResizeDiskReplyMsg;
import com.gcloud.storage.service.impl.NodeVolumeServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class NodeResizeDiskHandler extends AsyncMessageHandler<NodeResizeDiskMsg> {

    @Autowired
    private NodeVolumeServiceImpl volumeService;

    @Autowired
    private MessageBus bus;

    @Override
    public void handle(NodeResizeDiskMsg msg) {
        NodeResizeDiskReplyMsg replyMsg = msg.deriveMsg(NodeResizeDiskReplyMsg.class);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        replyMsg.setVolumeId(msg.getVolumeId());
        try {
            log.info("正在resize存储卷：{}, {}", msg.getStorageType(), msg.getVolumeId());
            this.volumeService.resizeDisk(msg.getStorageType(), msg.getPoolName(), msg.getDriverName(), msg.getVolumeId(), msg.getOldSize(), msg.getNewSize());
            replyMsg.setSize(msg.getNewSize());
            replyMsg.setSuccess(true);
            log.info("resize存储卷成功：{}, {}", msg.getStorageType(), msg.getVolumeId());
        }
        catch (Exception ex) {
            log.error("::resize存储卷失�?", ex);
            replyMsg.setSuccess(false);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::resize存储卷失�?"));
        }
        this.bus.send(replyMsg);
    }

}