package com.gcloud.image.driver;

import com.gcloud.core.service.SpringUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ImageCacheDriverEnum {
	NODE("node", ImageCacheFileStoreDriverImpl.class),
	RBD("rbd", ImageCacheRbdStoreDriverImpl.class),
	VG("vg", ImageCacheVgStoreDriverImpl.class);

	private String storageType;
	private Class driverClazz;
	
	ImageCacheDriverEnum(String storageType, Class clazz){
		this.storageType = storageType;
		this.driverClazz = clazz;
	}
	
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	
	public Class getDriverClazz() {
		return driverClazz;
	}

	public void setDriverClazz(Class driverClazz) {
		this.driverClazz = driverClazz;
	}

	public static IImageCacheStoreDriver getByType(String type) {
		for (ImageCacheDriverEnum driverE : ImageCacheDriverEnum.values()) {
			if(driverE.getStorageType().equals(type)) {
				return (IImageCacheStoreDriver)SpringUtil.getBean(driverE.driverClazz);
			}
		}
		return null;
	}
}