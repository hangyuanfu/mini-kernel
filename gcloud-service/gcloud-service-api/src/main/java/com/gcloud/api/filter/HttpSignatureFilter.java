package com.gcloud.api.filter;

import com.gcloud.api.ApiIdentityConfig;
import com.gcloud.api.security.SecurityParameter;
import com.gcloud.common.constants.HttpRequestConstant;
import com.gcloud.common.util.SignCore;
import com.gcloud.common.util.SignUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.identity.IApiIdentity;
import com.gcloud.core.identity.SignUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class HttpSignatureFilter implements Filter {
	@Autowired
	IApiIdentity apiIdentity;
	@Autowired
	ApiIdentityConfig identityConfig;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		String reqPath = request.getRequestURI();
		String tokenId = request.getHeader(HttpRequestConstant.HEADER_TOKEN_ID);
		if(tokenId == null && !identityConfig.isTestMode()) {  

			if(!(StringUtils.isEmpty(request.getHeader(SecurityParameter.AccessKeyId.toString())) 
					|| StringUtils.isEmpty(request.getHeader(SecurityParameter.Signature.toString()))
					|| StringUtils.isEmpty(request.getHeader(SecurityParameter.Timestamp.toString())))) {
				long timestamp = Long.parseLong(request.getHeader(SecurityParameter.Timestamp.toString()));
    			long currentTimestamp = System.currentTimeMillis();
    			if(Math.abs(timestamp - currentTimestamp) > identityConfig.getApiTimeout() * 60 * 1000) {
    				log.debug("::非法api请求");
                    throw new GCloudException("::非法api请求");
    			}
    			
                String sig = request.getHeader("Signature");
                Map<String, String> paramMap = SignUtil.getAllParamAsString(request.getParameterMap());
    			Map<String, String> newParams = SignCore.paraFilter(paramMap);

    			String url = request.getRequestURL().toString();

    			String httpMethod = request.getMethod();

    			Map<String, String> headers = new HashMap<>();
    			headers.put("SignatureMethod", request.getHeader(SecurityParameter.SignatureMethod.toString()));
    			headers.put("AccessKeyId", request.getHeader(SecurityParameter.AccessKeyId.toString()));
    			headers.put("Timestamp", request.getHeader(SecurityParameter.Timestamp.toString()));
                
    			//User curUser = userService.findUniqueByProperty("accessKey", request.getHeader(SecurityParameter.AccessKeyId.toString()));
    			SignUser signUser= (SignUser) CacheContainer.getInstance().get(CacheType.SIGN_USER,request.getHeader(SecurityParameter.AccessKeyId.toString()));
    			if(signUser==null)
    				signUser=apiIdentity.getUserByAccessKey(request.getHeader(SecurityParameter.AccessKeyId.toString()));
    			if(signUser==null) {
    				log.debug("::非法api请求");
                    throw new GCloudException("::非法api请求");
    			}
                boolean isValid = false;
    			//通过认证获取secretKey
    			try {
    				isValid = SignUtil.verify(sig, httpMethod, url, null, newParams, new HashMap<String, String>(), signUser.getSecretKey());
    			}catch(Exception e) {
    				log.error("api签名失败" +e.getMessage());
    				throw new GCloudException("::系统异常，请联系管理�?");
    			}
    			if(!isValid) {
                	log.debug("::非法api请求");
                    throw new GCloudException("::非法api请求");
                } else {
//                	ModifiableHttpServletRequest modifyReq = new ModifiableHttpServletRequest(request);
//    	        	modifyReq.addParameter("currentUser.id", signUser.getUserId());
    	        	
    	        	request.setAttribute(HttpRequestConstant.ATTR_USER_INFO, signUser.getUserInfo());
    	        	
    	        	chain.doFilter(request, servletResponse);
                }
            } else if(!AuthFilter.getExcludedUrls().contains(reqPath)) {
            	log.debug("::非法api请求");
                throw new GCloudException("::非法api请求");
            } else {
    	    	chain.doFilter(servletRequest, servletResponse);
    	    }
	    } else {
	    	chain.doFilter(servletRequest, servletResponse);
	    }
	}

	@Override
	public void destroy() {

	}

}