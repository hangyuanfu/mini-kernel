package com.gcloud.controller.storage.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.model.DescribeStoragePoolsParams;
import com.gcloud.controller.storage.model.DetailPoolParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.storage.StorageErrorCodes;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class StoragePoolDao extends JdbcBaseDaoImpl<StoragePool, String> {

    public StoragePool checkAndGet(String zoneId, String categoryId) throws GCloudException {

        StringBuffer sql = new StringBuffer();
        sql.append("select p.* from gc_storage_pools p, gc_disk_category_pools t");
        sql.append(" where p.id = t.storage_pool_id");
        sql.append(" and t.zone_id = ? and t.disk_category_id = ?");

        List<Object> values = new ArrayList<>();
        values.add(zoneId);
        values.add(categoryId);

        List<StoragePool> res = this.findBySql(sql.toString(), values);
        if (res.isEmpty()) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_POOL);
        }
        return res.get(0);
    }

    public StoragePool checkAndGet(String poolId) throws GCloudException {
        StoragePool pool = this.getById(poolId);
        if (pool == null) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_POOL);
        }
        return pool;
    }

    public <E> PageResult<E> describeStoragePools(int pageNumber, int pageSize, String poolId, Class<E> clazz) {
        StringBuilder sql = new StringBuilder();
        List<Object> values = new ArrayList<>();
        sql.append("SELECT * FROM gc_storage_pools");
        
        if(StringUtils.isNotBlank(poolId)) {
        	sql.append(" where id = ?");
        	values.add(poolId);
        }
        
        sql.append(" ORDER BY pool_name ASC");
        return this.findBySql(sql.toString(), values, pageNumber, pageSize, clazz);
    }

    public <E> PageResult<E> describeStoragePools(DescribeStoragePoolsParams params, Class<E> clazz) {
        StringBuilder sql = new StringBuilder();
        List<Object> values = new ArrayList<>();
        sql.append("SELECT * FROM gc_storage_pools where 1 = 1 ");
        
        if(StringUtils.isNotBlank(params.getPoolId())) {
        	sql.append(" and id = ?");
        	values.add(params.getPoolId());
        }
        
        if(StringUtils.isNotBlank(params.getStorageType())) {
        	StorageType type = StorageType.value(params.getStorageType());
        	if(null == type) {
        		throw new GCloudException("::无法识别该存储类�?");
        	}
        	sql.append(" and storage_type = ?");
        	values.add(params.getStorageType());
        }
        
        if(StringUtils.isNotBlank(params.getZoneId())) {
        	sql.append(" and id in (select spz.storage_pool_id from gc_storage_pool_zones as spz where spz.zone_id = ?)");
        	values.add(params.getZoneId());
        }

        if(StringUtils.isNotBlank(params.getPoolName())){
            sql.append(" and pool_name like concat('%', ?, '%')");
            values.add(params.getPoolName());
        }

        if(StringUtils.isNotBlank(params.getDisplayName())){
            sql.append(" and display_name like concat('%', ?, '%')");
            values.add(params.getDisplayName());
        }

        if(StringUtils.isNotBlank(params.getHostname())){
            sql.append(" and hostname like concat('%', ?, '%')");
            values.add(params.getHostname());
        }
        
        //过滤已经绑定的，传了这个参数必须要有zoneId和磁盘类型ID
        if(params.getAssociated() != null && params.getAssociated().booleanValue()) {
        	if(StringUtils.isNotBlank(params.getAssociateDiskCategoryId()) && StringUtils.isNotBlank(params.getAssociateZoneId())) {
        		sql.append("and id not in (select storage_pool_id from gc_disk_category_pools where disk_category_id = ? and zone_id = ?)");
        		values.add(params.getAssociateDiskCategoryId());
        		values.add(params.getAssociateZoneId());
        	}
        }
        
        sql.append(" ORDER BY pool_name ASC");
        return this.findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
    }
    
    public StoragePool find(Integer provider, String storageType, String poolName, String hostname) {
        Map<String, Object> props = new HashMap<>();
        props.put(StoragePool.PROVIDER, provider);
        props.put(StoragePool.STORAGE_TYPE, storageType);
        props.put(StoragePool.POOL_NAME, poolName);
        if(StorageType.LOCAL.getValue().equals(storageType)){
            props.put(StoragePool.HOSTNAME, hostname);
        }
        List<StoragePool> res = this.findByProperties(props);
        return res.isEmpty() ? null : res.get(0);
    }
    
    public <E> List<E> detailPool(DetailPoolParams params, CurrentUser currentUser, Class<E> clazz) {
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select p.*, zone.id as zone_id, zone.name as zone_name from gc_storage_pools as p ");
    	sql.append(" left join gc_storage_pool_zones as spz on p.id = spz.storage_pool_id ");
    	sql.append(" left join gc_zones as zone on zone.id = spz.zone_id ");
    	sql.append(" where p.id = ?");
    	values.add(params.getPoolId());
    	
    	return findBySql(sql.toString(), values, clazz);
    }
    
    public List<StoragePool> findPoolWithoutZoneByHostname(String hostname) {
    	if(StringUtils.isBlank(hostname)) {
    		return null;
    	}
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select * from gc_storage_pools where hostname = ? ");
    	sql.append(" and id not in (select storage_pool_id from gc_storage_pool_zones)");
    	values.add(hostname);
    	
    	return findBySql(sql.toString(), values);
    }

    public List<String> storagePoolStorageType(){
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct t.storage_type from gc_storage_pools t order by t.storage_type");

        return this.jdbcTemplate.queryForList(sql.toString(), String.class);
    }
    
    public List<StoragePool> getStoragePoolsByTypes(String[] storageTypes) {
        StringBuilder sql = new StringBuilder();
        List<Object> values = new ArrayList<>();
        sql.append("SELECT * FROM gc_storage_pools where 1=1 ");
        if(storageTypes.length > 0) {
        	sql.append(" and (");
        	for(int i=0;i<storageTypes.length;i++) {
        		String storageType = storageTypes[i];
        		sql.append(i==0?" storage_type=? ":"or storage_type=? ");
        		values.add(storageType);
        	}
        	
        	sql.append(" )");
        }
        
        return findBySql(sql.toString(), values);
    }

}