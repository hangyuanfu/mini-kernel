package com.gcloud.controller.storage.handler.api.volume;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.storage.workflow.DeleteDiskWorkflow;
import com.gcloud.controller.storage.workflow.model.volume.DeleteDiskInitFlowCommandRes;
import com.gcloud.controller.storage.workflow.model.volume.DeleteDiskWorkflowReq;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.log.model.Task;
import com.gcloud.header.storage.msg.api.volume.ApiDeleteDiskMsg;

import org.springframework.beans.factory.annotation.Autowired;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@LongTask
@GcLog(isMultiLog = true, taskExpect = "删除磁盘")
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DeleteDisk")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.VOLUME, resourceIdField = "diskId")
public class ApiDeleteDiskHandler extends BaseWorkFlowHandler<ApiDeleteDiskMsg, ApiReplyMessage> {

    @Autowired
    private IVolumeService volumeService;

    /*@Override
    public ApiReplyMessage handle(ApiDeleteDiskMsg msg) throws GCloudException {
        volumeService.deleteVolume(msg.getDiskId(), msg.getTaskId());
        msg.setObjectId(msg.getDiskId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, msg.getDiskId()));
        return new ApiReplyMessage();
    }*/

	@Override
	public Object preProcess(ApiDeleteDiskMsg msg) throws GCloudException {
		Volume vol = volumeService.getVolume(msg.getDiskId());
		if(vol == null) {
			throw new GCloudException(":: 该磁盘不存在");
		}
		return null;
	}

	@Override
	public ApiReplyMessage process(ApiDeleteDiskMsg msg) throws GCloudException {
		ApiReplyMessage replyMessage = new ApiReplyMessage();
		DeleteDiskInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DeleteDiskInitFlowCommandRes.class);
		replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getDiskId())
        		.objectName(CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, msg.getDiskId())).expect("删除磁盘").build());

        return replyMessage;
	}
	
	@Override
    public Object initParams(ApiDeleteDiskMsg msg) {
		DeleteDiskWorkflowReq req = new DeleteDiskWorkflowReq();
        req.setVolumeId(msg.getDiskId());
        req.setDeleteSnapshot(true);
        return req;
    }

	@Override
	public Class getWorkflowClass() {
		return DeleteDiskWorkflow.class;
	}
}