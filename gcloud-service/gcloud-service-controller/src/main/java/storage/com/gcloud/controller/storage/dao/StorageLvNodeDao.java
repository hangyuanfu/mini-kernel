package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.StorageLvNode;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Jdbc("controllerJdbcTemplate")
@Repository
public class StorageLvNodeDao extends JdbcBaseDaoImpl<StorageLvNode, Integer>{
	public List<StorageLvNode> getLvNodes(String lvPath) {
		List<Object> values = new ArrayList<>();

        StringBuffer sql = new StringBuffer();
        sql.append("select * from gc_storage_lv_nodes where lv_path=? order by state asc");
        values.add(lvPath);

        return findBySql(sql.toString(), values, StorageLvNode.class);
	}
	
	public List<StorageLvNode> findByImageId(String imageId){
		List<Object> values = new ArrayList<>();

        StringBuffer sql = new StringBuffer();
        sql.append("select * from gc_storage_lv_nodes where lv_path like ? order by state asc");
        values.add("%/img-" + imageId);

        return findBySql(sql.toString(), values, StorageLvNode.class);
	}
}