package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.model.AssociateDiskCategoryZoneParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiAssociateDiskCategoryZoneMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="磁盘类型关联可用�?")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.ASSOCIATE_DISK_CATEGORY_ZONE, name = "磁盘类型关联可用�?")
public class ApiAssociateDiskCategoryZoneHandler extends MessageHandler<ApiAssociateDiskCategoryZoneMsg, ApiReplyMessage>{

	@Autowired
    private IStoragePoolService poolService;
	
	@Override
	public ApiReplyMessage handle(ApiAssociateDiskCategoryZoneMsg msg) throws GCloudException {
		AssociateDiskCategoryZoneParams params = BeanUtil.copyBean(msg, AssociateDiskCategoryZoneParams.class);
		poolService.associateDiskCategoryZone(params);
		
		ApiReplyMessage reply = new ApiReplyMessage();
		
		//添加操作日志
		DiskCategory diskCategory = poolService.getDiskCategoryById(msg.getDiskCategoryId());
		String diskCategoryName = null;
		if(diskCategory != null) {
			diskCategoryName = diskCategory.getName();
		}

		msg.setObjectId(msg.getDiskCategoryId());
		msg.setObjectName(diskCategoryName);
		return reply;
	}

}