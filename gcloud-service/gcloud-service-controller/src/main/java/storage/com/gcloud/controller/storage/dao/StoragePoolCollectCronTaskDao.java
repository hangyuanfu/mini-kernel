package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.StoragePoolCollectCronTask;
import com.gcloud.controller.storage.enums.CollectStorageInfoState;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
public class StoragePoolCollectCronTaskDao extends JdbcBaseDaoImpl<StoragePoolCollectCronTask, String>{
	
	public String getLastedTask() {
		//List<Object> values = new ArrayList<Object>();
		//String sql = "select batch_id from gc_storage_pool_collect_cron_tasks where state=? order by batch_id desc limit 0,1";
		//优先获取已收集完成的�?新批�?
		String sql = "select batch_id from gc_storage_pool_collect_cron_tasks ORDER BY state asc,batch_id desc LIMIT 0,1";
		//values.add(CollectStorageInfoState.COLLECTED.value());
		List<String> tasks = this.jdbcTemplate.queryForList(sql, String.class);
		return tasks.size()>0?tasks.get(0):null;
	}
	
	public void setPreTaskCollected(String curBatchId) {
		String sql = "update gc_storage_pool_collect_cron_tasks set state = ? where state = ? and batch_id !=?";
        Object[] values = {CollectStorageInfoState.COLLECTED.value(), CollectStorageInfoState.COLLECTING.value(), curBatchId};

        this.jdbcTemplate.update(sql, values);
	}
	
	public void deleteHistoryBatch() {
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_storage_pool_collect_cron_tasks where state=?");
		values.add(CollectStorageInfoState.COLLECTED.value());
		
		this.jdbcTemplate.update(sql.toString(), values.toArray());
	}
}