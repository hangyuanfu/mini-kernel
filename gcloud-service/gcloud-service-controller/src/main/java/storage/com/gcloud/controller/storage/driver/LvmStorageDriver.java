package com.gcloud.controller.storage.driver;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.StorageLvNodeDao;
import com.gcloud.controller.storage.dao.StorageNodeDao;
import com.gcloud.controller.storage.dao.StoragePoolNodeDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StorageLvNode;
import com.gcloud.controller.storage.entity.StorageNode;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.StoragePoolNode;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.ResourceProviderVo;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.log.enums.LogType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.StoragePoolInfo;
import com.gcloud.header.storage.msg.node.volume.NodeCreateDiskMsg;
import com.gcloud.header.storage.msg.node.volume.NodeCreateSnapshotMsg;
import com.gcloud.header.storage.msg.node.volume.NodeDeleteDiskMsg;
import com.gcloud.header.storage.msg.node.volume.NodeDeleteSnapshotMsg;
import com.gcloud.header.storage.msg.node.volume.NodeResetSnapshotMsg;
import com.gcloud.header.storage.msg.node.volume.NodeResizeDiskMsg;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.lvm.uitls.LvmUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
public class LvmStorageDriver implements IStorageDriver{
	@Autowired
    private SnapshotDao snapshotDao;
	
	@Autowired
    private IVolumeService volumeService;
	
	@Autowired
    private VolumeDao volumeDao;
	
	@Autowired
    private StorageLvNodeDao storageLvNodeDao;
	
	@Autowired
    private StoragePoolNodeDao storagePoolNodeDao;
	
	@Autowired
	private MessageBus bus;
	
	@Override
	public StorageType storageType() {
		return StorageType.CENTRAL;
	}

	@Override
	public void createStoragePool(String poolId, String poolName, String hostname, String taskId)
			throws GCloudException {
		//创建VG�?3种方式，raid、分区�?�整个磁盘，以下将使用�?�分�?/dev/sdb1�? �? “磁�?/dev/sdc�? 创建VG，并设置PE大小�?64M
		// vgcreate poolName /dev/sdb1 /dev/sdc -s 64M
		
	}

	@Override
	public void deleteStoragePool(String poolName) throws GCloudException {
		//vgdisplay -v VolGroup05
		//umount�?有lv�?
		// vgchange -a n VolGroup05
		//vgremove VolGroup05
		//vgscan  验证是否已经删除
		//编辑/etc/fstab，删除对应挂载信�?
		
	}

	@Override
	public StoragePoolInfo getStoragePool(StoragePool pool) throws GCloudException {
		//vgdisplay -v VolGroup05
		return null;
	}

	@Override
	public CreateDiskResponse createVolume(String taskId, StoragePool pool, Volume volume) throws GCloudException {
		NodeCreateDiskMsg msg = new NodeCreateDiskMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setPoolName(volume.getPoolName());
        msg.setDriverName(pool.getDriver());
        msg.setVolumeId(volume.getProviderRefId());
        msg.setSize(volume.getSize());
        msg.setImageId(volume.getImageRef());
        msg.setSnapshotId(volume.getSnapshotId());
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), null)));
        msg.setTaskId(taskId);
        this.bus.send(msg);

        CreateDiskResponse response = new CreateDiskResponse();
        response.setLogType(LogType.ASYNC);
        response.setDiskId(volume.getId());
        return response;
        
		/*// lvcreate -L 2500 -n volumeName poolName
		//创建了块设备/dev/poolName/volumeName
		boolean lvCreate = LvmUtil.lvCreate(pool.getPoolName(), "volume-" + volume.getId(), volume.getSize(), "::创建块设�?" + LvmUtil.getVolumePath(pool.getPoolName(), volume.getId()) + "失败");
		
        if (volume.getImageRef() == null) {
        	LvmUtil.activeLv(LvmUtil.getVolumePath(pool.getPoolName(), volume.getId()));
        	//配置数据盘的大小�?100G（即云服务器里第二块盘的空间大小�?
    		//qemu-img create -f qcow2 /dev/poolName/volumeName 100G
        	DiskQemuImgUtil.create(volume.getSize(), "G", LvmUtil.getVolumePath(pool.getPoolName(), volume.getId()), "qcow2");
        } else {
        	//判断镜像在节点上是否�?�?
        	//如果不是�?活，则进行激活操�?
        	LvmUtil.activeLv(LvmUtil.getImagePath(pool.getPoolName(), volume.getImageRef()));
        	
        	//qemu-img create -b /dev/poolName/imageId -f qcow2 /dev/poolName/volumeName
        	DiskQemuImgUtil.create(LvmUtil.getImagePath(pool.getPoolName(), volume.getImageRef()), LvmUtil.getVolumePath(pool.getPoolName(), volume.getId()), "qcow2");
        }
        
        this.volumeDao.updateVolumeStatus(volume.getId(), VolumeStatus.AVAILABLE);

        CreateDiskResponse response = new CreateDiskResponse();
        response.setLogType(LogType.SYNC);
        response.setDiskId(volume.getId());
        return response;*/
	}

	@Override
	public void deleteVolume(String taskId, StoragePool pool, Volume volume) throws GCloudException {
		NodeDeleteDiskMsg msg = new NodeDeleteDiskMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setDriverName(pool.getDriver());
        msg.setPoolName(volume.getPoolName());
        msg.setVolumeId(volume.getProviderRefId());
        for (Snapshot snapshot : this.snapshotDao.findByVolume(volume.getId())) {
            msg.getSnapshots().add(new ResourceProviderVo(snapshot.getId(), snapshot.getProviderRefId()));
        }
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), volume.getId())));
        msg.setTaskId(taskId);
        this.bus.send(msg);
        
		//umount /dev/poolName/volumeName -- 这步�?要吗�?
		//lvremove /dev/poolName/volumeName
		//lvdisplay | grep "/dev/poolName/volumeName"
		/*LvmUtil.remove(pool.getPoolName(), "volume-" + volume.getId());
        this.volumeService.handleDeleteVolumeSuccess(volume.getId());
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, volume.getId());*/
	}

	@Override
	public void resizeVolume(String taskId, StoragePool pool, Volume volume, int newSize) throws GCloudException {
		
		NodeResizeDiskMsg msg = new NodeResizeDiskMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setPoolName(volume.getPoolName());
        msg.setDriverName(pool.getDriver());
        msg.setVolumeId(volume.getProviderRefId());
        msg.setOldSize(volume.getSize());
        msg.setNewSize(newSize);
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), volume.getId())));
        msg.setTaskId(taskId);
        this.bus.send(msg);
        
		/*// lvextend -L +10G /dev/poolName/volumeName
		//e2fsck /dev/poolName/volumeName  [强烈建议先不�?-f]  扫描文件信息 --这步�?要吗�?
		//resize2fs /dev/poolName/volumeName  --这步�?要吗�?
		
		//缩小�?要以下步�?
		//umount 挂载�?
		//resize2fs /dev/poolName/volumeName 2G 
		//lvreduce -L 2G /dev/poolName/volumeName
		//mount
		LvmUtil.extend(pool.getPoolName(), "volume-" + volume.getId(), newSize-volume.getSize());
		DiskQemuImgUtil.resize(getDiskPath(pool.getPoolName(), "volume-" + volume.getId()), newSize-volume.getSize());
		this.volumeService.handleResizeVolumeSuccess(volume.getId(), newSize);
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, volume.getId());*/
	}

	@Override
	public void createSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, String taskId)
			throws GCloudException {
		NodeCreateSnapshotMsg msg = new NodeCreateSnapshotMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setPoolName(pool.getPoolName());
        msg.setDriverName(pool.getDriver());
        msg.setVolumeRefId(volumeRefId);
        msg.setSnapshotId(snapshot.getId());
        msg.setSnapshotRefId(snapshot.getProviderRefId());
        msg.setTaskId(taskId);
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), volumeRefId)));
        this.bus.send(msg);
		/*//lvcreate -s -l 100 -n volumeNameSnap /dev/poolName/volumeName
		//-s    关键选项，创建快照snap的意�?    
	    //-l    后面跟快照包含多少个PE的数�?
	    //-n    后面跟创建的快照的名�?
	    //-p r  由于快照大多为只读，改�?�项为为修改权限位只读（r�?
		
		//lvdisplay /dev/poolName/volumeNameSnap
		//LvmUtil.snap(pool.getPoolName(), "volume-" + volumeRefId, snapshot.getId());
		//改用内部快照
		DiskQemuImgUtil.snapshot(snapshot.getId(), getDiskPath(pool.getPoolName(), "volume-" + volumeRefId));
		LongTaskUtil.syncSucc(LogType.SYNC, taskId, null);*/
	}

	@Override
	public void deleteSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, String taskId)
			throws GCloudException {
		NodeDeleteSnapshotMsg msg = new NodeDeleteSnapshotMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setPoolName(pool.getPoolName());
        msg.setDriverName(pool.getDriver());
        msg.setVolumeRefId(volumeRefId);
        msg.setSnapshotId(snapshot.getId());
        msg.setSnapshotRefId(snapshot.getProviderRefId());
        msg.setTaskId(taskId);
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), volumeRefId)));
        this.bus.send(msg);
		/*// lvremove /dev/poolName/volumeNameSnap 
		//LvmUtil.remove(pool.getPoolName(), "volume-" + volumeRefId);
		DiskQemuImgUtil.deleteSnapshot(snapshot.getId(), getDiskPath(pool.getPoolName(), "volume-" + volumeRefId));
		LongTaskUtil.syncSucc(LogType.SYNC, taskId, snapshot.getId());*/
	}

	@Override
	public void resetSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, Integer size, String taskId)
			throws GCloudException {
		NodeResetSnapshotMsg msg = new NodeResetSnapshotMsg();
        msg.setStorageType(this.storageType().getValue());
        msg.setPoolName(pool.getPoolName());
        msg.setDriverName(pool.getDriver());
        msg.setVolumeRefId(volumeRefId);
        msg.setSnapshotId(snapshot.getId());
        msg.setSnapshotRefId(snapshot.getProviderRefId());
        msg.setSize(size);
        msg.setTaskId(taskId);
        msg.setServiceId(MessageUtil.storageServiceId(getStorageHostName(pool.getId(), pool.getPoolName(), volumeRefId)));
        this.bus.send(msg);
        
		/*// lvconvert --merge /dev/poolName/volumeNameSnap
		//LvmUtil.convert(pool.getPoolName(), snapshot.getId());
		//恢复快照后，快照会被自动删除掉，程序�?要将数据库中对应的快照记录删除？
		
		DiskQemuImgUtil.resetSnapshot(snapshot.getId(), getDiskPath(pool.getPoolName(), "volume-" + volumeRefId));
		LongTaskUtil.syncSucc(LogType.SYNC, taskId, snapshot.getId());*/
	}
	
	private String getStorageHostName(String poolId, String poolName, String volumeId) {
		if(StringUtils.isNotBlank(volumeId)) {
			List<StorageLvNode> storageLvNodes = storageLvNodeDao.getLvNodes(LvmUtil.getVolumePath(poolName, volumeId));
			if(storageLvNodes.size() > 0) {
				return storageLvNodes.get(0).getHostname();
			}
		}
		List<StoragePoolNode> storagePoolNodes = storagePoolNodeDao.getStoragePoolNodes(poolId);
		if(storagePoolNodes.size() > 0) {
			return storagePoolNodes.get(0).getHostname();
		} else {
			throw new GCloudException("::该存储池没有合�?�的节点");
		}
	}

}