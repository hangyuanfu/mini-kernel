package com.gcloud.controller.storage.async.volume;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.controller.storage.provider.impl.GcloudVolumeProvider;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.log.enums.LogType;
import com.gcloud.header.storage.enums.VolumeStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Slf4j
public class GcloudCreateDiskAsyncTask {
	@Autowired
    private VolumeDao volumeDao;
	
	@Async("asyncExecutor")
    public void createVolumeAsync(String volumeId, CreateVolumeParams params, CurrentUser currentUser) throws GCloudException {
    	log.debug(String.format("createVolumeAsync %s start %s,线程ID�?%s",volumeId, new Date().getTime(), Thread.currentThread().getId()));
        Volume volume = new Volume();
        {
            volume.setId(volumeId);
            volume.setDisplayName(StringUtils.isBlank(params.getDiskName())?volumeId:params.getDiskName());
            volume.setStatus(VolumeStatus.CREATING.value());
            volume.setSize(params.getSize());
            volume.setDiskType(params.getDiskType().getValue());
            volume.setCreatedAt(new Date());
            volume.setDescription(params.getDescription());
            volume.setSnapshotId(params.getSnapshotId());
            volume.setUserId(currentUser.getId());
            volume.setTenantId(currentUser.getDefaultTenant());
            volume.setCategory(params.getDiskCategory());
            volume.setStorageType(params.getPool().getStorageType());
            volume.setPoolId(params.getPool().getId());
            volume.setPoolName(params.getPool().getPoolName());
            volume.setProvider(params.getPool().getProvider());
            volume.setProviderRefId(volume.getId());
            volume.setZoneId(params.getZoneId());
            volume.setImageRef(params.getImageProviderRefId());
        }
        this.volumeDao.save(volume);
        try{
        	CreateDiskResponse response = GcloudVolumeProvider.DRIVERS.get(volume.getStorageType()).createVolume(params.getTaskId(), params.getPool(), volume);
            LongTaskUtil.syncSucc(response.getLogType(), params.getTaskId(), volumeId);
        }catch (Exception ex){
            //没有采用事务，直接回�?
            volumeDao.deleteById(volumeId);
            LongTaskUtil.syncFail(LogType.SYNC, params.getTaskId(), volumeId);
        }
        log.debug(String.format("createVolumeAsync %s end %s",volumeId, new Date().getTime()));
    }
}