package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.controller.storage.workflow.model.volume.CreateSnapshotFlowCommandReq;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class CreateSnapshotFlowCommand extends BaseWorkFlowCommand{
    
    @Autowired
    private ISnapshotService snapshotService;
    
	@Override
	protected Object process() throws Exception {
		CreateSnapshotFlowCommandReq req = (CreateSnapshotFlowCommandReq)getReqParams();
		snapshotService.createSnapshot(req.getDiskId(), req.getName(), req.getDescription(), req.getCurrentUser(), getTaskId(), req.getSnapshotId());
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		CreateSnapshotFlowCommandReq req = (CreateSnapshotFlowCommandReq)getReqParams();
		snapshotService.deleteSnapshot(req.getSnapshotId(), getTaskId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CreateSnapshotFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}