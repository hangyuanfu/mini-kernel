package com.gcloud.controller.storage.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVolumeProvider extends IResourceProvider {

    CreateDiskResponse createVolume(String volumeId, CreateVolumeParams params, CurrentUser currentUser) throws GCloudException;

    void deleteVolume(StoragePool pool, Volume volume, String taskId) throws GCloudException;

    String attachVolume(Volume volume, String gcAttachmentId, String instanceId, String mountPoint, String hostname, String taskId) throws GCloudException;

    void updateVolume(String volumeRefId, String name, String description) throws GCloudException;

    void reserveVolume(String volumeRefId) throws GCloudException;

    void unreserveVolume(String volumeRefId) throws GCloudException;

    void beginDetachingVolume(Volume volume) throws GCloudException;

    void detachVolume(Volume volume, VolumeAttachment attachment) throws GCloudException;

    void rollDetachingVolume(Volume volume) throws GCloudException;

    void resizeVolume(StoragePool pool, Volume volume, int newSize, String taskId) throws GCloudException;

    List<Volume> getVolumeList(Map<String, String> filterParams) throws GCloudException;

    List<Snapshot> getSnapshotList(Map<String, String> filterParams) throws GCloudException;

}