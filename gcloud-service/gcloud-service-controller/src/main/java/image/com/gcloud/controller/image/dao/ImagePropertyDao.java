package com.gcloud.controller.image.dao;

import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class ImagePropertyDao extends JdbcBaseDaoImpl<ImageProperty, Long>{

    public int deleteByImageId(String imageId){

        String sql = "delete from gc_image_properties where image_id = ?";
        Object[] values = {imageId};
        return this.jdbcTemplate.update(sql, values);
    }

}