package com.gcloud.controller.image.handler.api.standard;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.image.model.DescribeImageParams;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.enums.standard.StandardImageStatus;
import com.gcloud.header.image.model.ImageType;
import com.gcloud.header.image.model.standard.StandardImageType;
import com.gcloud.header.image.msg.api.standard.StandardApiDescribeImagesMsg;
import com.gcloud.header.image.msg.api.standard.StandardApiDescribeImagesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DescribeImages", versions = {ApiVersion.Standard}, name = "镜像列表")
public class StandardApiDescribeImagesHandler extends MessageHandler<StandardApiDescribeImagesMsg, StandardApiDescribeImagesReplyMsg>{

	@Autowired
    private IImageService imageService;
	
	@Override
	public StandardApiDescribeImagesReplyMsg handle(StandardApiDescribeImagesMsg msg) throws GCloudException {
		DescribeImageParams params = toParams(msg);
        if(msg.getDisable() == null) {
        	params.setDisable(null);
        }
        PageResult<ImageType> response = imageService.describeImage(params, msg.getCurrentUser());
        PageResult<StandardImageType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        
        StandardApiDescribeImagesReplyMsg reply = new StandardApiDescribeImagesReplyMsg();
        reply.init(stdResponse);

        return reply;
	}
	
	public List<StandardImageType> toStandardReply(List<ImageType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardImageType> stdList = new ArrayList<>();
		for (ImageType data : list) {
			StandardImageType stdData = BeanUtil.copyBean(data, StandardImageType.class);
			stdData.setStatus(StandardImageStatus.standardStatus(data.getStatus()));
			stdList.add(stdData);
		}
		return stdList;
	}
	
	public DescribeImageParams toParams(StandardApiDescribeImagesMsg msg) {
		DescribeImageParams params = BeanUtil.copyProperties(msg, DescribeImageParams.class);
		
		if(StringUtils.isNotBlank(msg.getStatus())) {
			StandardImageStatus stdStatus = StandardImageStatus.value(msg.getStatus());
			if(null == stdStatus) {
				throw new GCloudException("::不支持该状�?�的查询");
			}
			if(stdStatus.getGcStatus() == null || stdStatus.getGcStatus().size() == 1) {
				params.setStatues(null);
				params.setStatus(stdStatus.getGcStatusValues().get(0));
			} else {
				params.setStatus(null);
				params.setStatues(stdStatus.getGcStatusValues());
			}
		}
		return params;
	}

}