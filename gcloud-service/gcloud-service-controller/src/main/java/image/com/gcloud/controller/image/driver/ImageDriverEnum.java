package com.gcloud.controller.image.driver;

import com.gcloud.core.service.SpringUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ImageDriverEnum {
	FILE("file", FileStoreDriver.class),
	RBD("rbd", RbdStoreDriver.class);

	private String storageType;
	private Class clazz;
	
	ImageDriverEnum(String storageType, Class clazz){
		this.storageType = storageType;
		this.clazz = clazz;
	}
	
	public static IImageStoreDriver getByType(String type) {
		for (ImageDriverEnum driverE : ImageDriverEnum.values()) {
			if(driverE.getStorageType().equals(type)) {
				return (IImageStoreDriver)SpringUtil.getBean(driverE.clazz);
			}
		}
		return null;
	}
	
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public Class getClazz() {
		return clazz;
	}
	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}
	
}