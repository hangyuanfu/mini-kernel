package com.gcloud.controller.image.provider;

import java.util.List;
import java.util.Map;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.model.iso.CreateIsoParams;
import com.gcloud.controller.image.model.iso.DistributeIsoParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IIsoProvider extends IResourceProvider{
	String createIso(CreateIsoParams params, CurrentUser currentUser) throws GCloudException;

    void updateIso(String isoId, String imageProviderRefId, String isoName) throws GCloudException;

    void deleteIso(String isoId, String imageProviderRefId) throws GCloudException;

    List<Iso> listIso(Map<String, String> filters) throws GCloudException;

    void distributeIso(DistributeIsoParams params);
}