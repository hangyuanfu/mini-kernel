package com.gcloud.controller.image.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_image_stores", jdbc = "controllerJdbcTemplate")
public class ImageStore {
	@ID
    private String id;
    private String imageId;
    private String storeTarget;
    private String storeType;
    private String status;//downloading\deleting\active
    private String resourceType;//image or iso
    
    public static final String ID = "id";
    public static final String IMAGE_ID = "imageId";
    public static final String STORE_TARGET = "storeTarget";
    public static final String STORE_TYPE = "storeType";
    public static final String STATUS = "status";
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getStoreTarget() {
		return storeTarget;
	}
	public void setStoreTarget(String storeTarget) {
		this.storeTarget = storeTarget;
	}
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateImageId(String imageId) {
        this.setImageId(imageId);
        return IMAGE_ID;
    }

    public String updateStoreTarget(String storeTarget) {
        this.setStoreTarget(storeTarget);
        return STORE_TARGET;
    }
    
    public String updateStoreType(String storeType) {
        this.setStoreType(storeType);
        return STORE_TYPE;
    }

    public String updateStatus(String status) {
        this.setStatus(status);
        return STATUS;
    }
}