package com.gcloud.controller.image.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.model.DetailImage;
import com.gcloud.header.image.msg.api.ApiDetailImageMsg;
import com.gcloud.header.image.msg.api.ApiDetailImageReplyMsg;
import com.gcloud.controller.image.model.DetailImageParams;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DetailImage",name="镜像详情")
public class ApiDetailImageHandler extends MessageHandler<ApiDetailImageMsg, ApiDetailImageReplyMsg>{
	@Autowired
	private IImageService imageService;
	 
	@Override
	public ApiDetailImageReplyMsg handle(ApiDetailImageMsg msg) throws GCloudException {
		DetailImageParams params = BeanUtil.copyProperties(msg, DetailImageParams.class);
		DetailImage response = imageService.detailImage(params, msg.getCurrentUser());
		
		ApiDetailImageReplyMsg repley = new ApiDetailImageReplyMsg();
		repley.setDetailImage(response);
		return repley;
	}

}