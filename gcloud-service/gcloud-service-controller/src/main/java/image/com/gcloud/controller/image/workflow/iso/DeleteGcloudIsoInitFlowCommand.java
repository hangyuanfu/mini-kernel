package com.gcloud.controller.image.workflow.iso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.controller.image.workflow.model.iso.DeleteGcloudIsoInitFlowCommandReq;
import com.gcloud.controller.image.workflow.model.iso.DeleteGcloudIsoInitFlowCommandRes;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.image.enums.ImageStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class DeleteGcloudIsoInitFlowCommand extends BaseWorkFlowCommand{
	
	@Autowired
	VmIsoAttachmentDao vmIsoAttachmentDao;
	
	@Autowired
	IsoDao isoDao;
	
	@Override
	protected Object process() throws Exception {
		DeleteGcloudIsoInitFlowCommandReq req = (DeleteGcloudIsoInitFlowCommandReq)getReqParams();
		Iso iso = isoDao.getById(req.getIsoId());
		if(iso == null) {
			throw new GCloudException("0090702::找不到对应映�?");
		}
		if(iso.getStatus().equals(ImageStatus.SAVING.value()) || iso.getStatus().equals(ImageStatus.PENDING_DELETE.value())) {
			throw new GCloudException("0090703::该状态的映像不能删除");
		}
		List<VmIsoAttachment> vmIsoAttachments = vmIsoAttachmentDao.findByProperty("isoId", req.getIsoId());
		if(vmIsoAttachments.size() > 0) {
			throw new GCloudException("0090704::有云服务器正在使用此映像，不能删�?");
		}
		
		iso.setStatus(ImageStatus.PENDING_DELETE.value());
		isoDao.update(iso);
		
		DeleteGcloudIsoInitFlowCommandRes res = new DeleteGcloudIsoInitFlowCommandRes();
        res.setTaskId(getTaskId());
        return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return DeleteGcloudIsoInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return DeleteGcloudIsoInitFlowCommandRes.class;
	}

}