package com.gcloud.controller.timer;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.controller.dao.MessageTaskDao;
import com.gcloud.controller.entity.MessageTask;
import com.gcloud.controller.enums.MessageTaskStatus;
import com.gcloud.core.async.AsyncThreadPool;
import com.gcloud.core.handle.MessageHandlerKeeper;
import com.gcloud.core.handle.MessageTimeoutHandler;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import com.gcloud.header.NodeMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@QuartzTimer(fixedDelay = 10 * 1000L)
@Slf4j
public class MessageTaskTimeoutTimer extends GcloudJobBean {

    @Autowired
    private MessageTaskDao messageTaskDao;

    @Autowired
    private AsyncThreadPool threadPool;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        List<MessageTask> taskList = messageTaskDao.timeoutTask();
        if(taskList == null || taskList.size() == 0){
            return;
        }

        for(MessageTask task : taskList){
            int result = messageTaskDao.finish(task.getTaskId(), MessageTaskStatus.TIMEOUT);
            if(result <= 0){
                continue;
            }
            //只处理了NodeMessage
            NodeMessage message = null;
            try{

                JSONObject mdata = JSONObject.parseObject(task.getMdata());
                String clazzName = mdata.getString("class");
                Class clazz = Class.forName(clazzName);
                message = (NodeMessage) JSONObject.parseObject(mdata.getString("data"), clazz);

            }catch (Exception ex){
                log.error("转成Message失败, ex=" + ex, ex);
            }

            if(message == null){
                continue;
            }

            MessageTimeoutHandler timeoutHandler = MessageHandlerKeeper.getTimeoutHandler(message.getClass().getName());
            if(timeoutHandler == null){
                continue;
            }

            threadPool.getExecutor().submit(new TimeoutHandlerThread(timeoutHandler, message));
        }


    }
}