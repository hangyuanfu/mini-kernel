package com.gcloud.controller.utils;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class OrderBy implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String field;
    private OrderType orderType;

    protected OrderBy(String field, OrderType orderType) {
        this.field = field;
        this.orderType = orderType;
    }

    public static OrderBy asc(String field) {
        return new OrderBy(field, OrderType.ASC);
    }

    public static OrderBy desc(String field) {
        return new OrderBy(field, OrderType.DESC);
    }

    public enum OrderType {
        ASC, DESC;

        public String value(){
            return name();
        }

    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public String toSqlString() {
        return toSqlString(false, false);
    }

    public String toSqlString(boolean IsOutSqlKeyword) {
        return toSqlString(IsOutSqlKeyword, false);
    }

    public String toSqlString(boolean IsOutSqlKeyword, boolean isFrist) {
        return (IsOutSqlKeyword ? (isFrist ? "order by " : ", ") : "") + field + " " + (orderType.equals(OrderType.DESC) ? "desc" : "asc");
    }

}