package com.gcloud.controller.provider;


import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gcloud.common.model.PortRangeInfo;
import com.gcloud.common.util.NetworkUtil;
import com.gcloud.controller.network.model.AllocateEipAddressResponse;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.provider.enums.neutron.NeutronSecurityDirection;
//import com.gcloud.controller.slb.provider.enums.NeutronSchedulerPolicy;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.EtherType;
import com.gcloud.header.network.model.AllocationPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.network.*;
import org.openstack4j.model.network.builder.*;
import org.openstack4j.model.network.ext.*;
import org.openstack4j.model.network.options.PortListOptions;
import org.openstack4j.openstack.OSFactory;
import org.openstack4j.openstack.networking.domain.ext.ListItem;
import org.openstack4j.openstack.networking.domain.ext.NeutronLoadBalancerV2.LoadBalancerV2ConcreteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.openstack4j.model.network.PhysicalNetwork;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
public class NeutronProviderProxy extends OpenstackProviderProxy{

	@Autowired
	//NeutronProvider provider;
	public void setProvider(NeutronProvider provider) {
		// TODO Auto-generated method stub
		super.setProvider(provider);
	}


	public static void main(String[] args){


//		System.setProperty("http.proxyHost", "127.0.0.1");
//		System.setProperty("https.proxyHost", "127.0.0.1");
//		System.setProperty("http.proxyPort", "8888");h
//		System.setProperty("https.proxyPort", "8888");

		System.out.println("===========begin======");
		OSClientV3 client=OSFactory.builderV3().endpoint("http://192.168.203.30:35357/v3")
				.credentials("admin", "j0p4cs2CUSOFdly6W9bjQUrlE9TU4bVVXIpU6NTL", Identifier.byName("Default"))
				.scopeToProject(Identifier.byName("admin"), Identifier.byName("Default"))
				.authenticate();

		System.out.println("===========token========" + client.getToken().getId());

		Object obj = null;
//		 = client.networking().router().list();
//		obj = client.networking().network().create(Builders.network().name("test").build());
//		obj = client.networking().network().list();

		LoadBalancerV2 lb = Builders.loadbalancerV2().name("lbabc").subnetId("ef65e6ae-be10-4fd3-acb5-06a43d3af7b4").adminStateUp(true).build();
		obj = client.networking().lbaasV2().loadbalancer().create(lb);



		System.out.println(JSONObject.toJSONString(obj, SerializerFeature.PrettyFormat));


	}

	public Network createExternalNetwork(String name, String networkType, String physicalNetwork, Integer segmentId){
		NetworkBuilder networkBuilder = Builders.network()
                .name(name)
                .isRouterExternal(true)
                .isShared(true)
                .adminStateUp(true)
                .networkType(NetworkType.forValue(networkType))
        		.physicalNetwork(physicalNetwork);
		if(segmentId != null) {
			networkBuilder.segmentId(segmentId.toString());
		}
		return createNetwork(networkBuilder.build());
	}

	public Network createNetwork(String name){

		Network network = Builders.network()
                .name(name).adminStateUp(true)
                .build();
		return createNetwork(network);
	}

	public Network createNetwork(Network network){
		try {
			network = getClient().networking().network().create(network);
		} catch(Exception ex) {
			log.error("创建网络失败:" + ex.getMessage(), ex);
			throw new GCloudException("::创建网络失败");
		}

		if(network == null){
			log.error("创建网络失败，返回null");
			throw new GCloudException("::创建网络失败");
		}

		return network;
	}

	public void deleteNetwork(String networkId) {
		ActionResponse response = null;
		try {
			response = getClient().networking().network().delete(networkId);
		} catch(Exception ex) {
			log.error(ex.getMessage(), ex);
			throw new GCloudException("::删除网络失败");
		}

		if(response == null){
			log.error("删除网络失败, response is null");
			throw new GCloudException("::删除网络失败");
		}

		if(!response.isSuccess()){
			log.error(String.format("删除网络失败,message=%s,code=%s,networkId=%s", response.getFault(), response.getCode(), networkId));
			throw new GCloudException("::删除网络失败");
		}
	}

	public Network getExternalNetwork(String id){
		return getClient().networking().network().get(id);
	}

	public void removeExternalNetwork(String id){
		deleteNetwork(id);
	}

	public void updateExternalNetwork(String id,String name){
		NetworkUpdate updater=Builders.networkUpdate().name(name).build();
		Network newNetwork = null;
		try {
			newNetwork = getClient().networking().network().update(id, updater);
		} catch(Exception ex) {
			log.error("更新网络失败:" + ex.getMessage(), ex);
			throw new GCloudException("::修改外网网络失败");
		}

		if(newNetwork == null){
			log.error("更新网络失败，返回null");
			throw new GCloudException("::更新网络失败");
		}
	}

	public Subnet createSubnet(String networkId, String name, String crid, List<String> dnsNameservers, String gatewayIp, boolean dhcp, List<AllocationPool> allocationPools){
		SubnetBuilder builder = Builders.subnet().networkId(networkId).name(name);
		builder.ipVersion(IPVersionType.V4).cidr(crid).enableDHCP(dhcp);
		if(com.gcloud.common.util.StringUtils.isNotBlank(gatewayIp)) {
			builder.gateway(gatewayIp);
		}
		if (dnsNameservers != null) {
			for (String host : dnsNameservers) {
				builder.addDNSNameServer(host);
			}
		}

		if(allocationPools != null){
			for(AllocationPool pool : allocationPools){
				builder.addPool(pool.getStart(), pool.getEnd());
			}
		}

		Subnet subnet = null;
		try {
			subnet = getClient().networking().subnet().create(builder.build());
		} catch(Exception ex) {
			log.error("创建子网失败" + ex.getMessage(), ex);
			throw new GCloudException("::创建子网失败");
		}

		if(subnet == null){
			log.error("创建子网失败，返回null");
			throw new GCloudException("::创建子网失败");
		}

		return subnet;
	}
	public void deleteSubnet(String id){
		ActionResponse response = getClient().networking().subnet().delete(id);
		if(response == null){
			log.error("删除子网失败, response is null, id=" + id);
			throw new GCloudException("::删除子网失败");
		}

		if(!response.isSuccess()){
			log.error(String.format("删除子网失败,message=%s,code=%s,subnetId=%s", response.getFault(), response.getCode(), id));
			throw new GCloudException("::删除子网失败");
		}
	}

	public Subnet getSubnet(String subnetId){
		return getClient().networking().subnet().get(subnetId);
	}

	public Subnet modifySubnetAttribute(String subnetId,String subnetName, List<String> dnsNameservers, String gatewayIp, Boolean dhcp) {

		//如果要修改dhcp，需要清除数据库dhcp的port

		//获取当前dhcp值，dhcp不能设置为null

		SubnetBuilder builder = Builders.subnet();
		if(dhcp != null){
			builder.enableDHCP(dhcp);
		}else{
			Subnet subnet = getSubnet(subnetId);
			builder.enableDHCP(subnet.isDHCPEnabled());
		}

		builder.name(subnetName);
		if(com.gcloud.common.util.StringUtils.isNotBlank(gatewayIp)) {
			builder.gateway(gatewayIp);
		}
		if (dnsNameservers != null) {
			if(dnsNameservers.size() == 0){
				builder.clearDNSNameServer();
			}else{
				for (String host : dnsNameservers) {
					builder.addDNSNameServer(host);
				}
			}
		}
		Subnet newSubnet = null;
		try{
			newSubnet = getClient().networking().subnet().update(subnetId, builder.build());
        }catch (Exception ex){
            log.error(String.format("更新子网失败，id=%s, ex=%s", subnetId, ex.getMessage()), ex);
            throw new GCloudException("::更新子网失败");
        }

		if (newSubnet == null) {
            log.error(String.format("更新子网失败, 返回为null，id=%s", subnetId));
            throw new GCloudException("::更新子网失败");
		}

		return newSubnet;
	}

	public Port createPort(String name, String securityGroup, String subnetId, String primaryIp){
        return createPort(name, securityGroup, subnetId, primaryIp, null, null, null, null);
	}

	public Port createPort(String name, String securityGroup, String subnetId, String primaryIp, String deviceId, String deviceOwner, String bindingHost, Boolean portSecurityEnabled){
		PortBuilder portBuilder = Builders.port();

		Subnet subnet = getSubnet(subnetId);
		if (subnet == null) {
			throw new GCloudException("subnet is null");
		}

		if(StringUtils.isNotBlank(name)){
			portBuilder.name(name);
		}

		portBuilder.networkId(subnet.getNetworkId());
		if(StringUtils.isNotBlank(primaryIp)){
			portBuilder.fixedIp(primaryIp, subnetId);
		}else{
			portBuilder.fixedIp(null, subnetId);
		}

		if(StringUtils.isNotBlank(deviceId)){
			portBuilder.deviceId(deviceId);
		}

		if(StringUtils.isNotBlank(deviceOwner)){
			portBuilder.deviceOwner(deviceOwner);
		}

		if(StringUtils.isNotBlank(bindingHost)){
			portBuilder.hostId(bindingHost);
		}

		if(portSecurityEnabled != null){
			portBuilder.portSecurityEnabled(portSecurityEnabled);
		}
		//启用安全�?
		if(portSecurityEnabled == null || portSecurityEnabled){
			if(StringUtils.isNotBlank(securityGroup)){
				portBuilder.securityGroup(securityGroup);
			}
		}


		return createPort(portBuilder.build());
	}

	public Port createPort(Port createPort){
        Port port = null;
        try{
            port = getClient().networking().port().create(createPort);
        }catch (Exception ex){
            log.error("创建端口失败, " + ex, ex);
            throw new GCloudException("::创建端口失败");
        }

        if (port == null) {
            log.error("创建端口失败，port �? null");
            throw new GCloudException("::创建端口失败");
        }

		return port;
	}
	
	public void deletePort(String id){
        ActionResponse response = null;
        try{
            response = getClient().networking().port().delete(id);
        }catch(Exception ex){
            log.error("删除端口失败" + ex, ex);
            throw new GCloudException("::删除端口失败");
        }

        if(response == null){
            log.error(String.format("删除端口失败,response为空,portId=%s", id));
            throw new GCloudException("::删除端口失败");
        }

        if (!response.isSuccess()) {
            log.error(String.format("删除端口失败,message=%s,code=%s,portId=%s", response.getFault(), response.getCode(), id));
            throw new GCloudException("::删除端口失败");
        }

	}
	
	public SecurityGroup createSecurityGroup(String name, String description){
		NetSecurityGroupBuilder builder = Builders.securityGroup().name(name).description(description);

		SecurityGroup securityGroup = null;
		try{
			securityGroup = getClient().networking().securitygroup().create(builder.build());
		}catch (Exception ex){
			log.error("创建安全组失�?" + ex, ex);
			if(ex.getMessage().indexOf("Quota exceeded for resources: ['security_group']") != -1) {
				throw new GCloudException("::超过安全组限额，创建安全组失�?");
			}
			throw new GCloudException("::创建安全组失�?");
		}

		if(securityGroup == null){
			log.error("创建安全组失败，返回null");
			throw new GCloudException("::创建安全组失�?");
		}

		return securityGroup;
	}
	
	public void removeSecurityCroup(String id){
		ActionResponse response = getClient().networking().securitygroup().delete(id);

		if(response == null){
			log.error(String.format("删除安全组失�?,response为空,id=%s", id));
			throw new GCloudException("::删除安全组失�?");
		}

		if(!response.isSuccess()){
			log.error(String.format("删除安全组失�?,id=%s,message=%s,code=%s", id, response.getFault(), response.getCode()));
			throw new GCloudException("::删除安全组失�?");
		}
	}
	
	public SecurityGroup getSecurityCroup(String id){
		return getClient().networking().securitygroup().get(id);
	}
	
	public Router createRouter(String name){
		Router router = null;

		try {
			router = getClient().networking().router().create(Builders.router().name(name).build());
		} catch (Exception ex) {
			log.error("::创建路由器失�?" + ex, ex);
			throw new GCloudException("::创建路由器失�?");
		}

		if (router == null) {
			log.error("创建路由器失败，router �? null");
			throw new GCloudException("::创建路由器失�?");
		}
		
		return router;
	}
	
	public void deleteRouter(String routerId){
		ActionResponse response = null;
        try{
            response = getClient().networking().router().delete(routerId);
        }catch(Exception ex){
            log.error("删除路由失败" + ex, ex);
            throw new GCloudException("0020403::删除路由失败");
        }

        if(response == null){
            log.error(String.format("删除路由失败,response为空,routerId=%s", routerId));
            throw new GCloudException("0020404::删除端口失败");
        }

        if (!response.isSuccess()) {
            log.error(String.format("删除路由失败,message=%s,code=%s,routerId=%s", response.getFault(), response.getCode(), routerId));
            throw new GCloudException("0020405::删除路由失败");
        }
	}

	public void updateRouter(Router router){
		Router newRouter = null;
		try{
			newRouter = getClient().networking().router().update(router);
		} catch(Exception ex) {
			log.error(String.format("更新路由失败: id = %s, ex = %s", router.getId(), ex.getMessage()), ex);
			
			String cannotCleanGateway = "Gateway cannot be updated for router , since a gateway to external network  is required by one or more floating IPs.";
			String error = ex.getMessage();
			error = error.substring(0, error.indexOf(".")+1);
			if(error.replaceAll("[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}", "").equals(cannotCleanGateway)) {
				throw new GCloudException("::有浮动IP在使用，无法清除网关");
			}else {
				throw new GCloudException("::修改外网网络失败");
			}
		}

		if(newRouter == null){
			log.error("更新路由失败，返回null, id = " + router.getId());
			throw new GCloudException("::更新网络失败");
		}
	}

	public void updateRouter(String routerId, String routerName){
		Router router = getRouter(routerId);
		if(router == null){
			throw new GCloudException("::路由不存�?");
		}
		router.toBuilder().name(routerName);
		updateRouter(router);
	}
	
	public Router getRouter(String routerId) {
		return getClient().networking().router().get(routerId);
	}
	
	public void setVRouterGateway(String routerId, String vpcId){
		Router router = getRouter(routerId);
		// 更新路由
		RouterBuilder routerBuilder = Builders.router().from(router).externalGateway(vpcId);
		updateRouter(routerBuilder.build());

	}
	
	public void cleanVRouterGateway(String routerId){
		Router router = getRouter(routerId);
		RouterBuilder builder = Builders.router().from(router).clearExternalGateway();
		updateRouter(builder.build());
	}



	public RouterInterface attachSubnetRouter(String routerId, String subnetId){
		RouterInterface routerInterface = null;
		try{
			routerInterface = getClient().networking().router().attachInterface(routerId, AttachInterfaceType.SUBNET, subnetId);
		}catch (Exception ex){
			log.error(String.format("绑定子网和路由失�?: routerId = %s, subnetId = %s,  ex=%s", routerId, subnetId, ex.getMessage()), ex);
			throw new GCloudException("::绑定子网和路由失�?");
		}

		if(routerInterface == null){
			log.error(String.format("绑定子网和路由失�?, 返回为null, routerId=%s, subnetId=%s", routerId, subnetId));
			throw new GCloudException("::绑定子网和路由失�?");
		}

		return routerInterface;
	}
	
	public void detachSubnetRouter(String routerId, String subnetId){
		detachSubnetRouter(routerId, subnetId, null);
	}
	
	public void detachSubnetRouter(String routerId, String subnetId, String portId){
		RouterInterface routerInterface = null;
		try{
			routerInterface = getClient().networking().router().detachInterface(routerId, subnetId, portId);
		}catch (Exception ex){
			log.error(String.format("解除路由和子网绑定失�?: routerId = %s, subnetId = %s, portId=%s,  ex=%s", routerId, subnetId, portId, ex.getMessage()), ex);
			throw new GCloudException("::解除路由和子网绑定失�?");
		}

		if(routerInterface == null){
			log.error(String.format("解除路由和子网绑定失�?, 返回为null, routerId=%s, subnetId=%s, portId=%s", routerId, subnetId, portId));
			throw new GCloudException("::解除路由和子网绑定失�?");
		}
	}
	
	public void allocate(){
		
	}
	
	public void deallocate(){
		
	}
	
	public Port getPort(String portId){
		return getClient().networking().port().get(portId);
	}
	
	public Port updatePort(Port port){
		Port newPort = null;
		try{
			newPort = getClient().networking().port().update(port);
		}catch (Exception ex){
			log.error(String.format("更新端口失败, portId=%s, ex=%s", port.getId(), ex.getMessage()), ex);
			throw new GCloudException("::更新端口失败");
		}

		if(newPort == null){
			log.error(String.format("更新端口失败,返回为null, portId=%s", port.getId()));
			throw new GCloudException("::更新端口失败");
		}
		return newPort;
	}
	
	public void modifySecurityGroupAttribute(String securityGroupId, String securityGroupName, String description) {

		SecurityGroup newSg = null;

		SecurityGroupUpdate updateBuilder = Builders.securityGroupUpdate().name(securityGroupName).description(description).build();
		try{
			newSg = getClient().networking().securitygroup().update(securityGroupId, updateBuilder);
		}catch (Exception ex){
			log.error(String.format("更新安全组失�?, securityGroupId=%s, ex=%s", securityGroupId, ex.getMessage()), ex);
			throw new GCloudException("::更新安全组失�?");
		}

		if(newSg == null){
			log.error(String.format("更新安全组失�?,返回为null, securityGroupId=%s", securityGroupId));
			throw new GCloudException("::更新安全组失�?");
		}

	}
	
	public String authorizeSecurityGroup(AuthorizeSecurityGroupParams params) {
		NetSecurityGroupRuleBuilder builder = Builders.securityGroupRule()
				.direction(NeutronSecurityDirection.getByGcValue(params.getDirection()))
				.securityGroupId(params.getSecurityGroupRefId())
				.ethertype(EtherType.IPv4.getValue());

		//处理all的情�?
		if("all".equalsIgnoreCase(params.getIpProtocol())) {
			builder.protocol(null);
		} else {
			builder.protocol(params.getIpProtocol());
		}

		if (params.getDirection().equals("egress")) {
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getDestGroupRefId())) {
				builder.remoteGroupId(params.getDestGroupRefId());
			}
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getDestCidrIp())) {
				builder.remoteIpPrefix(params.getDestCidrIp());
			}
		} else {
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getSourceGroupRefId())) {
				builder.remoteGroupId(params.getSourceGroupRefId());
			}
			if(com.gcloud.common.util.StringUtils.isNotBlank(params.getSourceCidrIp())) {
				builder.remoteIpPrefix(params.getSourceCidrIp());
			}
		}

		PortRangeInfo portRangeInfo = NetworkUtil.portRangeInfo(params.getPortRange());
		if(portRangeInfo == null){
			throw new GCloudException("ValidatePortRange.Wrong::端口范围参数范围不对或格式不�?");
		}else if(portRangeInfo.getMin() != null && portRangeInfo.getMin() != -1){
			builder.portRangeMin(portRangeInfo.getMin());
			builder.portRangeMax(portRangeInfo.getMax());
		}

		SecurityGroupRule rule = null;

		try{
			rule = getClient().networking().securityrule().create(builder.build());
		}catch (Exception ex){
			log.error(String.format("创建安全组规则失�?, ex=%s", ex.getMessage()), ex);
			throw new GCloudException("::创建安全组规则失�?");
		}

		if(rule == null){
			log.error("创建安全组规则失�?,返回为null");
			throw new GCloudException("::创建安全组规则失�?");
		}
		
		return rule.getId();
	}
	
	public void revokeSecurityGroup(String securityGroupRuleId) {
		try{
			getClient().networking().securityrule().delete(securityGroupRuleId);
		}catch (Exception ex){
			log.error(String.format("删除安全组规则失�?, ex=%s", ex.getMessage()), ex);
			throw new GCloudException("::删除安全组规则失�?");
		}

	}

	public List<? extends SecurityGroupRule> securityRuleList(String securityGroupId){
		Map<String, String> pars = new HashMap<>();
		pars.put("security_group_id", securityGroupId);
		return securityRuleList(pars);
	}

	public List<? extends SecurityGroupRule> securityRuleList(Map<String, String> pars){
		return getClient().networking().securityrule().list(pars);
	}

	public QosPolicy createQosPolicy(QosPolicy qosPolicy){
        QosPolicy newPolicy = null;
        try{
            newPolicy = getClient().networking().qosPolicies().create(qosPolicy);
        }catch (Exception ex){
            log.error("::创建qos策略失败", ex);
            throw new GCloudException("::创建qos策略失败");
        }

        if (newPolicy == null) {
            log.error("创建qos协议失败，new policy �? null");
            throw new GCloudException("::创建qos策略失败");
        }

        return newPolicy;

    }

	public void deleteQosPolicy(String id){
		ActionResponse response = null;
		try{
			response = getClient().networking().qosPolicies().delete(id);
		}catch(Exception ex){
			log.error("删除qos协议失败", ex);
			throw new GCloudException("::删除qos协议失败");
		}

		if(response == null){
			log.error(String.format("删除qos协议失败,response为空,portId=%s", id));
			throw new GCloudException("::删除qos协议失败");
		}

		if (!response.isSuccess()) {
			log.error(String.format("删除qos协议失败,message=%s,code=%s,portId=%s", response.getFault(), response.getCode(), id));
			throw new GCloudException("::删除qos协议失败");
		}

	}

    public QosBandwidthLimitRule createQosBandwidthLimitRule(String policyId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection){

        QosBandwidthLimitRule rule = null;
        try{
            rule = getClient().networking().qosBandwidthLimitRulus().create(policyId, maxKbps, maxBurstKbps, qosDirection);
        }catch (Exception ex){
            log.error("::创建qos带宽规则失败", ex);
            throw new GCloudException("::创建qos带宽规则失败");
        }

        if (rule == null) {
            log.error("创建qos带宽规则失败，rule �? null");
            throw new GCloudException("::创建qos带宽规则失败");
        }

        return rule;

    }

    public QosBandwidthLimitRule updateQosBandwidthLimitRule(String policyId, String ruleId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection){

        QosBandwidthLimitRule rule = null;
        try{
            rule = getClient().networking().qosBandwidthLimitRulus().update(policyId, ruleId, maxKbps, maxBurstKbps, qosDirection);
        }catch (Exception ex){
            log.error("::更新qos带宽规则失败", ex);
            throw new GCloudException("::更新qos带宽规则失败");
        }

        if (rule == null) {
            log.error("更新qos 带宽规则失败，rule �? null");
            throw new GCloudException("::更新qos带宽规则失败");
        }

        return rule;

    }

	public void deleteQosBandwidthLimitRule(String policyId, String ruleId){
		ActionResponse response = null;
		try{
			response = getClient().networking().qosBandwidthLimitRulus().delete(policyId, ruleId);
		}catch(Exception ex){
			log.error("删除qos带宽限制规则失败", ex);
			throw new GCloudException("::删除qos带宽限制规则失败");
		}

		if(response == null){
			log.error(String.format("删除qos带宽限制规则失败,response为空,policyId=%s,ruleId=%s", policyId, ruleId));
			throw new GCloudException("::删除qos带宽限制规则失败");
		}

		if (!response.isSuccess()) {
			log.error(String.format("删除qos带宽限制规则失败,message=%s,code=%s,policyId=%s,ruleId=%s", response.getFault(), response.getCode(), policyId, ruleId));
			throw new GCloudException("::删除qos带宽限制规则失败");
		}

	}

	public AllocateEipAddressResponse allocateEipAddress(String networkId) {
		NetFloatingIPBuilder builder = Builders.netFloatingIP().floatingNetworkId(networkId);
		NetFloatingIP floatingIP = null;
		try{
			floatingIP = getClient().networking().floatingip().create(builder.build());
		}catch (Exception ex){
			log.error(String.format("创建浮动IP失败, ex=%s", ex.getMessage()), ex);
			throw new GCloudException("::创建浮动IP失败");
		}

		if(floatingIP == null){
			log.error("创建浮动IP失败,返回为null");
			throw new GCloudException("::创建浮动IP失败");
		}

		AllocateEipAddressResponse response = new AllocateEipAddressResponse();
		response.setAllocationId(floatingIP.getId());
		response.setEipAddress(floatingIP.getFloatingIpAddress());
		response.setRouterId(floatingIP.getRouterId());
		response.setStatus(floatingIP.getStatus());
		
		return response;
	}

	public NetFloatingIP updateFloatingIp(NetFloatingIPUpdate update){
		NetFloatingIP newFloatingIp = null;
		try{
			newFloatingIp = getClient().networking().floatingip().update(update);
		}catch (Exception ex){
			log.error(String.format("更新浮动IP失败, fipId=%s, ex=%s", update.getId(), ex.getMessage()), ex);
			throw new GCloudException("::更新浮动IP失败");
		}

		if(newFloatingIp == null){
			log.error(String.format("更新浮动IP失败,返回为null, fipId=%s", update.getId()));
			throw new GCloudException("::更新浮动IP失败");
		}

		return newFloatingIp;
	}
	
	public NetFloatingIP associateEipAddress(String allocationId, String netcardId) {
		NetFloatingIP floatingIP = null;

		try{
			floatingIP = getClient().networking().floatingip().associateToPort(allocationId, netcardId);
		}catch (Exception ex){
			String cannotReachSubnet = "External network  is not reachable from subnet .";
			log.error(String.format("绑定浮动IP和网卡失�?, fipId=%s, portId=%s, ex=%s", allocationId, netcardId, ex.getMessage()), ex);
			String msg = ex.getMessage();
			msg = msg.substring(0, msg.indexOf(".")+1);
			if(msg.replaceAll("[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}", "").equals(cannotReachSubnet)) {
				throw new GCloudException("::外网网络与子网不通，绑定浮动IP和网卡失�?");
			}
			throw new GCloudException("::绑定浮动IP和网卡失�?");
		}

		if(floatingIP == null){
			log.error(String.format("绑定浮动IP和网卡失�?,返回为null, fipId=%s, portId=%s", allocationId, netcardId));
			throw new GCloudException("::绑定浮动IP和网卡失�?");
		}

		return floatingIP;
	}
	
	public NetFloatingIP unAssociateEipAddress(String allocationId) {
		NetFloatingIP floatingIP = null;

		try{
			floatingIP = getClient().networking().floatingip().disassociateFromPort(allocationId);
		}catch (Exception ex){
			log.error(String.format("解除浮动IP绑定失败, fipId=%s, ex=%s", allocationId, ex.getMessage()), ex);
			throw new GCloudException("::解除浮动IP绑定失败");
		}

		if(floatingIP == null){
			log.error(String.format("解除浮动IP绑定失败,返回为null, fipId=%s", allocationId));
			throw new GCloudException("::解除浮动IP绑定失败");
		}

		return floatingIP;
	}
	
	public void releaseEipAddress(String allocationId) {
		ActionResponse response = null;
		try{
			response = getClient().networking().floatingip().delete(allocationId);
		} catch(Exception ex) {
			log.error(String.format("删除浮动IP失败, id=%s, ex=%s", allocationId, ex.getMessage()), ex);
			throw new GCloudException("::删除浮动IP失败");
		}

		if(response == null){
			log.error("删除浮动IP失败, response is null, id=" + allocationId);
			throw new GCloudException("::删除浮动IP失败");
		}

		if(!response.isSuccess()){
			log.error(String.format("删除浮动IP失败,message=%s,code=%s,fipId=%s", response.getFault(), response.getCode(), allocationId));
			throw new GCloudException("::删除浮动IP失败");
		}

	}
	
	public LoadBalancerV2  createloadBalancer(String loadBalancerName,String vSwitchId) {
		LoadBalancerV2ConcreteBuilder builder=new LoadBalancerV2ConcreteBuilder();
		LoadBalancerV2 lb=builder.name(loadBalancerName).subnetId(vSwitchId).adminStateUp(true).build();
		try{
			lb=getClient().networking().lbaasV2().loadbalancer().create(lb);
		} catch(Exception ex) {
			log.error(String.format("创建负载均衡器失�?, vSwitchId=%s,lbname=%s, ex=%s", vSwitchId, loadBalancerName, ex.getMessage()), ex);
			throw new GCloudException("::创建负载均衡器失�?");
		}
		if(lb==null){
			throw new GCloudException("创建负载均衡器失�?");
		}
		return lb;
	}
	
	public  void   deleteLoadBalancer(String loadBalancerId) {
		ActionResponse resp = getClient().networking().lbaasV2().loadbalancer().delete(loadBalancerId);
		if (!resp.isSuccess()) {
//			throw new GCloudException("删除负载均衡器失�?:" + resp.getFault());
			throw new GCloudException(resp.getCode() + "::" + resp.getFault());
		}
		
	}
	
	public  LoadBalancerV2  getLoadBalancer(String loadBalancerId) {
		
		return  getClient().networking().lbaasV2().loadbalancer().get(loadBalancerId);
	}
	
	public void updateBalancerName(String loadBalancerId, String balancerName) {
		LoadBalancerV2 lber=getClient().networking().lbaasV2()
				.loadbalancer().update(loadBalancerId, Builders.loadBalancerV2Update().name(balancerName).build());
	}

	public List<LoadBalancerV2> listLoadBalancer(Map<String, String> filter) {
		List<LoadBalancerV2> lbs = (List<LoadBalancerV2>)getClient().networking().lbaasV2().loadbalancer().list(filter);
		return lbs;
	}

	public List<NetFloatingIP> listFloatingIps(Map<String, String> filter) {
		List<NetFloatingIP> fips = (List<NetFloatingIP>)getClient().networking().floatingip().list(filter);
		return fips;
	}

	public List<Network> listNetwork(Map<String, String> filter) {
		List<Network> networks = (List<Network>)getClient().networking().network().list(filter);
		return networks;
	}

	public List<SecurityGroup> listSecurityGroup(Map<String, String> filter) {
		List<SecurityGroup> sg = (List<SecurityGroup>)getClient().networking().securitygroup().list(filter);
		return sg;
	}

	public List<Port> listPort(Map<String, String> filter) {
		PortListOptions popt = PortListOptions.create();
		if (filter != null) {
			Set<String> set = filter.keySet();
			for (String key : set) {
			    String val = filter.get(key);
			    if (val == null || val.length() == 0) continue;
			    switch (key) {
					case "device_id": popt.deviceId(val); break;
					case "device_owner": popt.deviceOwner(val); break;
					case "network_id": popt.networkId(val); break;
					case "name": popt.name(val); break;
					case "tenant_id": popt.tenantId(val); break;
					case "mac_address": popt.macAddress(val); break;
					default: log.warn("::neutron端口列表，未知过滤参�?:" + key); break;
				}
			}
		}
		List<Port> ports = (List<Port>)listPort(popt);
		return ports;
	}

	public List<? extends Port> listPort(PortListOptions options){
		return getClient().networking().port().list(options);
	}

	public List<Subnet> listSubnet(Map<String, String> filter) {
		List<Subnet> subnets = (List<Subnet>)getClient().networking().subnet().list();
		return subnets;
	}

	public List<Router> listRouter() {
		List<Router> routers = (List<Router>)getClient().networking().router().list();
		return routers;
	}
	
	public ListenerV2 createLoadBalancerHTTPListener(String loadBalancerId, String vServerGroupId, Integer listenerPort) {

		ListenerV2 listener = getClient().networking().lbaasV2().listener()
                .create(Builders.listenerV2()
                   .protocol(ListenerProtocol.HTTP)
                   .protocolPort(listenerPort)
                   .loadBalancerId(loadBalancerId)
                   .defaultPoolId(vServerGroupId)
                   .adminStateUp(true)
                   .build());
		if(listener == null) {
			throw new GCloudException("::创建HTTP监听器失�?");
		}
		return listener;
		
	}
	
	public ListenerV2 createLoadBalancerHTTPSListener(String loadBalancerId, String vServerGroupId, Integer listenerPort, String ServerCertificateId) {

		ListenerV2 listener = getClient().networking().lbaasV2().listener()
                .create(Builders.listenerV2()
                   .protocol(ListenerProtocol.HTTPS)
                   .protocolPort(listenerPort)
                   .loadBalancerId(loadBalancerId)
                   .defaultPoolId(vServerGroupId)
                   .adminStateUp(true)
                   .build());
		if(listener == null) {
			throw new GCloudException("::创建HTTPS监听器失�?");
		}
		return listener;
		
	}
	
	public ListenerV2 createLoadBalancerTCPListener(String loadBalancerId, String vServerGroupId, Integer listenerPort) {

		ListenerV2 listener = getClient().networking().lbaasV2().listener()
				.create(Builders.listenerV2()
						.protocol(ListenerProtocol.TCP)
						.protocolPort(listenerPort)
						.loadBalancerId(loadBalancerId)
						.defaultPoolId(vServerGroupId)
						.adminStateUp(true)
						.build());
	
		if(listener == null) {
			throw new GCloudException("::创建TCP监听器失�?");
		}
		return listener;
		
	}
	
	public void deleteLoadBalancerListener(String listenerId) {
		
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		String poolId = listener.getDefaultPoolId();
		String monitorId = null;
		
		boolean hasMonitor = false;
		List<? extends HealthMonitorV2> healthMonitor = getClient().networking().lbaasV2().healthMonitor().list();
		
		//查询pool中对应的healthMonitor
		for (HealthMonitorV2 healthMonitorV2 : healthMonitor) {
			List<ListItem> pools = healthMonitorV2.getPools();
			for (ListItem pool : pools) {
				if(pool.getId().equals(poolId)) {
					hasMonitor = true;
					monitorId = healthMonitorV2.getId();
					break;
				}
			}
			if(hasMonitor) {
				break;
			}
		}
		
		if(hasMonitor) {
			if(StringUtils.isNotEmpty(monitorId)) {
				ActionResponse monitorResponse = getClient().networking().lbaasV2().healthMonitor().delete(monitorId);
				if(!monitorResponse.isSuccess()) {
					throw new GCloudException("::删除健康�?查失�?" + monitorResponse.getFault());
				}
			}
		}
		
		ActionResponse resp = getClient().networking().lbaasV2().listener().delete(listenerId);
		if (!resp.isSuccess()) {
			throw new GCloudException("::删除监听器失�?" + resp.getFault());
		}
		log.debug("DeleteListener end...");
		
	}
	
	public void setLoadBalancerHTTPListenerAttribute(String listenerId, String vServerGroupId) {
		
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		if (StringUtils.isNotBlank(vServerGroupId)) {	
			if (listener != null && !StringUtils.equals(listener.getDefaultPoolId(), vServerGroupId)) {
				ListenerV2 updated = getClient().networking().lbaasV2().listener()
						.update(listenerId, Builders.listenerV2Update()
								.defaultPoolId(vServerGroupId)
								.adminStateUp(true)
								.build());
				
				if (updated == null) {
					throw new GCloudException("::更新监听器失�?");
				}
			}
		}
		else {
			throw new GCloudException("::后端服务器组id不能为空");
		}
		
		log.debug("UpdateListener end...");
	}
	
	public void setLoadBalancerHTTPSListenerAttribute(String listenerId, String vServerGroupId, String serverCertificateId) {
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		if (StringUtils.isNotBlank(vServerGroupId)) {	
			if (listener != null && !StringUtils.equals(listener.getDefaultPoolId(), vServerGroupId)) {
				ListenerV2 updated = getClient().networking().lbaasV2().listener()
						.update(listenerId, Builders.listenerV2Update()
								.defaultPoolId(vServerGroupId)
								.adminStateUp(true)
								.build());
				
				if (updated == null) {
					throw new GCloudException("::更新监听器失�?");
				}
			}
		}
		else {
			throw new GCloudException("后端服务器组id不能为空");
		}
		
		log.debug("UpdateListener end...");
	}
	
	public void setLoadBalancerTCPListenerAttribute(String listenerId, String vServerGroupId) {

		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		if (StringUtils.isNotBlank(vServerGroupId)) {
			if (listener != null && !StringUtils.equals(listener.getDefaultPoolId(), vServerGroupId)) {
				ListenerV2 updated = getClient().networking().lbaasV2().listener()
						.update(listenerId, Builders.listenerV2Update()
								.defaultPoolId(vServerGroupId)
								.adminStateUp(true)
								.build());
				
				if (updated == null) {
					throw new GCloudException("::更新监听器失�?");
				}
			}
		}
		else {
			throw new GCloudException("后端服务器组id不能为空");
		}
		
		log.debug("UpdateTCPListener end...");
	}
	
	public ListenerV2 describeLoadBalancerHTTPListenerAttribute(String listenerId) {
	
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		
		if (listener == null) {
			throw new GCloudException("获取HTTP监听器详情失�?");
		}
		
		log.info("DescribeHTTPListener end...");
		
		return listener;
	}
	
	public ListenerV2 describeLoadBalancerHTTPSListenerAttribute(String listenerId) {
		
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		
		if (listener == null) {
			throw new GCloudException("::获取HTTP监听器详情失�?");
		}
		
		log.info("DescribeHTTPSListener end...");
		
		return listener;
	}
	
	public ListenerV2 describeLoadBalancerTCPListenerAttribute(String listenerId) {
		
		ListenerV2 listener = getClient().networking().lbaasV2().listener().get(listenerId);
		
		if (listener == null) {
			throw new GCloudException("::获取TCP监听器详情失�?");
		}
		
		log.info("DescribeTCPListener end...");
		
		return listener;
	}
	
	public List<? extends ListenerV2> describeLoadBalancerListenerByFilter(Map<String, String> filter) {
		List<? extends ListenerV2> list = getClient().networking().lbaasV2().listener().list(filter);
		return list;
	}
	
	public LbPoolV2 describeSchedulerAttribute(String resourceId, String protocol) {
		
		LbPoolV2 pool = getClient().networking().lbaasV2().lbPool().get(resourceId);
		if (pool == null) {
			throw new GCloudException("::获取Scheduler详情失败");
		}
		log.debug("DescribeSchedulerAttribute end...");
		
		return pool;
	}
	
	public void setSchedulerAttribute(String resourceId, String protocol, String scheduler) {

		if (StringUtils.isNotBlank(scheduler) && StringUtils.isNotBlank(resourceId)) {
			getClient().networking().lbaasV2().lbPool()
				.update(resourceId, Builders.lbPoolV2Update()
						.adminStateUp(true)
//						.lbMethod(LbMethod.forValue(scheduler))
						//.lbMethod(NeutronSchedulerPolicy.getByGCloudValue(scheduler))
						.build());
		}
		
		log.debug("SetSchedulerAttribute end...");
	}
	
	
}