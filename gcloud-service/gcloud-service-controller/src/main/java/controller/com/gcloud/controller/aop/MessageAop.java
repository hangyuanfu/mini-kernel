package com.gcloud.controller.aop;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.dao.MessageTaskDao;
import com.gcloud.controller.entity.MessageTask;
import com.gcloud.controller.enums.MessageTaskStatus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.GMessage;
import com.gcloud.header.NodeMessage;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Aspect
@Component
@Slf4j
public class MessageAop {

	@Autowired
	private MessageTaskDao messageTaskDao;

	@Pointcut("execution(* com.gcloud.core.messagebus.MessageBus.send(..))")
	public void messageSend() {
	}

	@Pointcut("execution(* com.gcloud.controller.MessageReceiver.proccessAsync(..))")
	public void messageReceive(){

	}

	@Before("messageSend()")
	public void doBefore(JoinPoint joinPoint) {

		try{
			Object[] args = joinPoint.getArgs();
			//只处理发送到node的消�?
			if(args == null || args.length == 0 || !NodeMessage.class.isAssignableFrom(args[0].getClass())){
				return;
			}

			NodeMessage nodeMessage = (NodeMessage)args[0];
			log.debug("message:%s");
			//只处理超时的
			if(StringUtils.isBlank(nodeMessage.getTaskId()) || nodeMessage.getTimeout() <= 0){
				return;
			}


			//只记录从controller发出�?
			//因为没有记录src，节点间不能相互发消息�?�所以先只要是发给controller的就不处理，后续优化
			if(MessageUtil.controllerServiceId().equals(nodeMessage.getServiceId())){
				return;
			}

			//记录到数据库，方便过滤查询，如果性能有问题再考虑放到redis
			MessageTask messageTask = new MessageTask();
			messageTask.setTaskId(nodeMessage.getTaskId());
			messageTask.setStatus(MessageTaskStatus.RUNNING.getValue());
			JSONObject mdata = new JSONObject();
			mdata.put("class", nodeMessage.getClass().getName());
			mdata.put("data", nodeMessage);
			messageTask.setMdata(mdata.toJSONString());
			messageTask.setMessage(nodeMessage.getClass().getSimpleName());

			Date starTime = new Date();
			messageTask.setStartTime(starTime);
			if(nodeMessage.getTimeout() > 0){
				messageTask.setTimeoutTime(new Date(starTime.getTime() + nodeMessage.getTimeout()));
			}

			messageTaskDao.save(messageTask);

		}catch (Exception ex){
			log.error("message aop error:" + ex, ex);
		}



	}

	//改成before，防止和timeout并发重复处理，如果需要用到task状�?�做判断，需要分�? before（防止并发） �? after（处理状态）
	@Before("messageReceive()")
	public void doAfter(JoinPoint joinPoint) {

		Object[] args = joinPoint.getArgs();
		//只处理发送到node的消�?
		if(args == null || args.length == 0){
			return;
		}

		Message message = (Message)args[0];
		GMessage gMessage = null;

		try{
			gMessage = (GMessage)SerializationUtils.deserialize(message.getBody());
		}catch (Exception ex){

		}

		if(gMessage == null || !NodeMessage.class.isAssignableFrom(gMessage.getClass())){
			return;
		}

		NodeMessage nodeMessage = (NodeMessage)gMessage;

		//只处理超时的
		if(StringUtils.isBlank(nodeMessage.getTaskId()) || nodeMessage.getTimeout() <= 0){
			return;
		}

		MessageTaskStatus taskStatus = MessageTaskStatus.SUCCESS;
		if(nodeMessage.getSuccess() == null || !nodeMessage.getSuccess()){
			taskStatus = MessageTaskStatus.FAILED;
		}

		messageTaskDao.finish(nodeMessage.getTaskId(), taskStatus);

	}


}