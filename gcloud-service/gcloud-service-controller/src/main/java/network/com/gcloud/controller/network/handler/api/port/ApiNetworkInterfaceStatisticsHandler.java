package com.gcloud.controller.network.handler.api.port;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS,action = "NetworkInterfaceStatistics",name="网卡统计")

public class ApiNetworkInterfaceStatisticsHandler extends MessageHandler<ApiNetworkInterfaceStatisticsMsg, ApiNetworkInterfaceStatisticsReplyMsg>{
	@Autowired
	private IPortService portService;
	
	@Override
	public ApiNetworkInterfaceStatisticsReplyMsg handle(ApiNetworkInterfaceStatisticsMsg msg) throws GCloudException {
		return portService.statistic(msg);
	}

}