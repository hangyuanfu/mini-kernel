package com.gcloud.controller.network.provider.enums.neutron;

import com.gcloud.header.compute.enums.DeviceOwner;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum  NeutronDeviceOwner {

    COMPUTE("compute:node", DeviceOwner.COMPUTE),
    FOREIGN("network:foreign", DeviceOwner.FOREIGN),
    DHCP("network:dhcp", DeviceOwner.DHCP),
    FLOATINGIP("network:floatingip", DeviceOwner.FLOATINGIP),
    ROUTER("network:router_interface", DeviceOwner.ROUTER),
    LOADBALANCER("neutron:LOADBALANCERV2", DeviceOwner.LOADBALANCER),
    GATEWAY("network:router_gateway", DeviceOwner.GATEWAY),
    HA_ROUTER("network:ha_router_replicated_interface", DeviceOwner.HA_ROUTER),
    ZX_FIREWALL("compute:zx_firewall", DeviceOwner.ZX_FIREWALL);

    private String neutronDeviceOwner;
    private DeviceOwner deviceOwner;

    NeutronDeviceOwner(String neutronDeviceOwner, DeviceOwner deviceOwner) {
        this.neutronDeviceOwner = neutronDeviceOwner;
        this.deviceOwner = deviceOwner;
    }

    public String getNeutronDeviceOwner() {
        return neutronDeviceOwner;
    }

    public DeviceOwner getDeviceOwner() {
        return deviceOwner;
    }

    public static String toGCloudValue(String neutronDeviceOwner){
        NeutronDeviceOwner deviceOwner = Arrays.stream(NeutronDeviceOwner.values()).filter(o -> o.getNeutronDeviceOwner().equals(neutronDeviceOwner)).findFirst().orElse(null);
        return deviceOwner == null ? null : deviceOwner.getDeviceOwner().getValue();
    }
    public static NeutronDeviceOwner getByGCloudValue(String gcloudDeviceOwner){
        return Arrays.stream(NeutronDeviceOwner.values()).filter(o -> o.getDeviceOwner().getValue().equals(gcloudDeviceOwner)).findFirst().orElse(null);
    }
}