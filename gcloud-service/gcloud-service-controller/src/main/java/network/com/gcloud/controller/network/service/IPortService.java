package com.gcloud.controller.network.service;

import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.model.CreatePortParams;
import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.network.model.DetailNetworkInterfaceParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.network.model.DetailNic;
import com.gcloud.header.network.model.NetworkInterfaceSet;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsReplyMsg;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IPortService {

    String create(CreatePortParams params, CurrentUser currentUser);

    void update(String portId, List<String> securityGroupIds, String portName);
    void updatePort(Port port);

    void delete(String portId);

    void updateQos(String portId, Integer egress, Integer ingress);
    void updatePortQos(String portId, Integer egress, Integer ingress);

    void attachPort(VmInstance instance, String portId, String customOvsBr, Boolean noArpLimit);

    PageResult<NetworkInterfaceSet> describe(DescribeNetworkInterfacesParams params, CurrentUser currentUser);

    VmNetworkDetail getNetworkDetail(String portId);

    void cleanPortData(String port);
    void createPortData(CreatePortParams params, ProviderType provider, CurrentUser currentUser, String portId, String portRefId, String macAddress, String state, String ipAddress);

    void detachDone(Port port);
    
    DetailNic detailNetworkInterface(DetailNetworkInterfaceParams params, CurrentUser currentUser);
    
    ApiNetworkInterfaceStatisticsReplyMsg statistic(ApiNetworkInterfaceStatisticsMsg msg);

    void migratePort(String portId, String sourceHostName, String targetHostname);

    void migratePortIgnoreBinding(String portId, String sourceHostName, String targetHostname);

    boolean hasAvailableIp(String subnetId);

}