package com.gcloud.controller.network.service;

import com.gcloud.controller.network.model.AllocateEipAddressResponse;
import com.gcloud.controller.network.model.AssociateEipAddressParams;
import com.gcloud.controller.network.model.ModifyEipAddressAttributeParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.DetailEipAddressResponse;
import com.gcloud.header.network.model.EipAddressSetType;
import com.gcloud.header.network.msg.api.ApiDetailEipAddressMsg;
import com.gcloud.header.network.msg.api.ApiEipAddressStatisticsReplyMsg;
import com.gcloud.header.network.msg.api.DescribeEipAddressesMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IFloatingIpService {
	AllocateEipAddressResponse allocateEipAddress(String networkId, Integer bandwidth, String regionId, CurrentUser currentUser);
	void associateEipAddress(AssociateEipAddressParams params);
	void unAssociateEipAddress(String allocationId);
	void releaseEipAddress(String allocationId);
	PageResult<EipAddressSetType> describeEipAddresses(DescribeEipAddressesMsg params);
	void modifyEipAddressAttribute(ModifyEipAddressAttributeParams param);
	DetailEipAddressResponse detail(ApiDetailEipAddressMsg msg);
	ApiEipAddressStatisticsReplyMsg statistic(CurrentUser currentUser);
}