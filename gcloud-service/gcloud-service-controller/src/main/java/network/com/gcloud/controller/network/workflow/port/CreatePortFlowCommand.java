package com.gcloud.controller.network.workflow.port;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.workflow.model.vm.CreateNetcardFlowCommandRes;
import com.gcloud.controller.network.model.CreatePortParams;
import com.gcloud.controller.network.model.workflow.CreatePortFlowCommandReq;
import com.gcloud.controller.network.model.workflow.CreatePortFlowCommandRes;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Scope("prototype")
@Slf4j
public class CreatePortFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private IPortService portService;


    @Override
    protected Object process() throws Exception {

        CreatePortFlowCommandReq req = (CreatePortFlowCommandReq)getReqParams();

        CreatePortParams params = new CreatePortParams();
        params.setSecurityGroupId(req.getSecurityGroupId());
        params.setIpAddress(req.getIpAddress());
        params.setSubnetId(req.getSubnetId());
        if(StringUtils.isNotBlank(req.getPortName())) {
        	params.setName(req.getPortName());
        }

        String portId = portService.create(params, req.getCreateUser());

        CreatePortFlowCommandRes res = new CreatePortFlowCommandRes();
        res.setPortId(portId);

        return res;
    }

    @Override
    protected Object rollback() throws Exception {

        CreatePortFlowCommandRes res = (CreatePortFlowCommandRes)getResParams();

        portService.delete(res.getPortId());

        return null;
    }

    @Override
    protected Object timeout() throws Exception {
    	CreateNetcardFlowCommandRes res = (CreateNetcardFlowCommandRes)getResParams();
		VmNetworkDetail vmNet = portService.getNetworkDetail(res.getNetcardId());
		if(null != vmNet) {
			return true;
		}
		return false;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return CreatePortFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return CreatePortFlowCommandRes.class;
    }
}