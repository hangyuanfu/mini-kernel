package com.gcloud.controller.network.dao;

import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class SubnetDhcpHandleDao extends JdbcBaseDaoImpl<SubnetDhcpHandle, String> {

    public void retry(String subnetId){
        String sql = "update gc_subnet_dhcp_handles set retry = ifnull(retry, 0) + 1 where subnet_id = ?";
        Object[] values = {subnetId};
        jdbcTemplate.update(sql, values);

    }

    public List<SubnetDhcpHandle> runningHandle(Integer maxRetry){

        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();
        sql.append("select * from gc_subnet_dhcp_handles where handle_id is not null and handle_id != ''");
        if(maxRetry != null && maxRetry > 0){
            sql.append(" and retry < ?");
            values.add(maxRetry);
        }

        return findBySql(sql.toString(), values);

    }

}