package com.gcloud.controller.network.handler.api.subnet.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.ISwitchService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateVSwitchMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateVSwitchMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateVSwitchReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建交换�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="CreateVSwitch", versions = {ApiVersion.Standard})
public class StandardApiCreateVSwitchHandler extends MessageHandler<StandardApiCreateVSwitchMsg, StandardApiCreateVSwitchReplyMsg>{

	@Autowired
	private ISwitchService switchService;
	
	@Override
	public StandardApiCreateVSwitchReplyMsg handle(StandardApiCreateVSwitchMsg msg) throws GCloudException {
		StandardApiCreateVSwitchReplyMsg reply = new StandardApiCreateVSwitchReplyMsg();
		reply.setvSwitchId(switchService.createVSwitch(toParams(msg), msg.getCurrentUser()));
		msg.setObjectId(reply.getvSwitchId());
		msg.setObjectName(msg.getvSwitchName());
		return reply;
	}
	
	public CreateVSwitchMsg toParams(StandardApiCreateVSwitchMsg msg) {
		CreateVSwitchMsg params = BeanUtil.copyProperties(msg, CreateVSwitchMsg.class);
		return params;
	}

}