package com.gcloud.controller.network.util;

import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.async.DeleteSubnetNeutronDhcpPortAsync;
import com.gcloud.controller.network.async.SyncNeutronDhcpPortAsync;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.dao.SubnetDhcpHandleDao;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.controller.network.provider.impl.NeutronPortProvider;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.service.ISubnetDhcpHandleService;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.service.common.Consts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class NeutronSubnetDhcpUtil {

    public static String dhcpLockKey(String subnetId){
        return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_NETWORK_DHCP_LOCK, subnetId);
    }

    public static String dhcpRunningKey(String subnetId){
        return MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_NETWORK_DHCP_RUNNING, subnetId);
    }

    public static SubnetDhcpHandleInfo canRun(String subnetId, String handleId){

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        SubnetDhcpHandle subnetDhcpHandle  = null;
        boolean caRun = false;

        try{
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            subnetDhcpHandle = dhcpHandlerDao.getById(subnetId);
            //已经被删
            if(subnetDhcpHandle == null){
                return null;
            }

            if(subnetDhcpHandle.getHandleId() == null || !subnetDhcpHandle.getHandleId().equals(handleId)){
                throw new GCloudException("::已经被其他处�?");
            }

            caRun = true;

        }catch (Exception ex){

        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        return new SubnetDhcpHandleInfo(caRun, subnetDhcpHandle, true);

    }

    public static SubnetDhcpHandleInfo syncDhcpPortData(String subnetId, String handleId, List<org.openstack4j.model.network.Port> neutronPorts){

        PortDao portDao = SpringUtil.getBean(PortDao.class);
        SubnetDao subnetDao = SpringUtil.getBean(SubnetDao.class);

        List<Port> gcloudPorts = portDao.subnetPorts(subnetId, DeviceOwner.DHCP.getValue());
        Map<String, Port> gcloudPortMap = new HashMap<>();
        if(gcloudPorts != null && gcloudPorts.size() > 0){
            gcloudPorts.stream().forEach(p -> gcloudPortMap.put(p.getProviderRefId(), p));
        }


        Map<String, org.openstack4j.model.network.Port> neutronPortMap = new HashMap<>();
        if(neutronPorts != null && neutronPorts.size() > 0){
            neutronPorts.stream().forEach(p -> neutronPortMap.put(p.getId(), p));
        }

        List<String> deletePorts = new ArrayList<>();
        deletePorts.addAll(gcloudPortMap.keySet());
        deletePorts.removeAll(neutronPortMap.keySet());

        List<String> createPorts = new ArrayList<>();
        createPorts.addAll(neutronPortMap.keySet());
        createPorts.removeAll(gcloudPortMap.keySet());

        IPortService portService = SpringUtil.getBean(IPortService.class);
        NeutronPortProvider portProvider = ResourceProviders.get(ResourceType.PORT, ProviderType.NEUTRON.getValue());

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        SubnetDhcpHandle subnetDhcpHandle  = null;
        boolean caRun = false;
        boolean operationSucc = true;

        try{
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            subnetDhcpHandle = dhcpHandlerDao.getById(subnetId);
            //已经被删
            if(subnetDhcpHandle == null){
                return null;
            }

            if(subnetDhcpHandle.getHandleId() == null || !subnetDhcpHandle.getHandleId().equals(handleId)){
                throw new GCloudException("::已经被其他处�?");
            }

            caRun = true;
        }catch (Exception ex){

        }

        try{

            for(String portRefId : deletePorts){
                Port port = gcloudPortMap.get(portRefId);
                portService.cleanPortData(port.getId());
            }

            if(createPorts.size() > 0){
                Subnet subnet = subnetDao.getById(subnetId);
                if(subnet != null){
                    for(String portRefId : createPorts){

                        org.openstack4j.model.network.Port neutronPort = neutronPortMap.get(portRefId);

                        Map<String, Object> portFilter = new HashMap<>();
                        portFilter.put(com.gcloud.controller.network.entity.Port.PROVIDER, ProviderType.NEUTRON.getValue());
                        portFilter.put(com.gcloud.controller.network.entity.Port.PROVIDER_REF_ID, portRefId);

                        com.gcloud.controller.network.entity.Port port = portDao.findOneByProperties(portFilter);

                        boolean sync = false;

                        if(port == null){
                            try{
                                portProvider.createPortData(neutronPort, null, subnet.getUserId(), subnet.getTenantId());
                            }catch (DuplicateKeyException dex){
                                log.debug("::端口已经创建");
                                sync = true;
                            }
                        }else{
                            sync = true;
                        }

                        if(sync){
                            portProvider.syncPortIp(neutronPort, port);
                        }
                    }
                }
            }

        }catch (Exception ex){
            operationSucc = false;
            log.error("subnet dhcp createDhcpPortData error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        return new SubnetDhcpHandleInfo(caRun, subnetDhcpHandle, operationSucc);


    }

    public static SubnetDhcpHandleInfo createDhcpPortData(String subnetId, String handleId, org.openstack4j.model.network.Port neutronPort, CurrentUser currentUser){

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        SubnetDhcpHandle subnetDhcpHandle  = null;
        boolean caRun = false;
        boolean operationSucc = true;

        try{
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            subnetDhcpHandle = dhcpHandlerDao.getById(subnetId);
            //已经被删
            if(subnetDhcpHandle == null){
                return null;
            }

            if(subnetDhcpHandle.getHandleId() == null || !subnetDhcpHandle.getHandleId().equals(handleId)){
                throw new GCloudException("::已经被其他处�?");
            }

            caRun = true;
        }catch (Exception ex){

        }


        try{
            NeutronPortProvider portProvider = ResourceProviders.get(ResourceType.PORT, ProviderType.NEUTRON.getValue());
            portProvider.createPortData(neutronPort, null, currentUser.getId(), currentUser.getDefaultTenant());
        }catch (Exception ex){
            operationSucc = false;
            log.error("subnet dhcp createDhcpPortData error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        return new SubnetDhcpHandleInfo(caRun, subnetDhcpHandle, operationSucc);

    }


    public static SubnetDhcpHandleInfo cleanDhcpPort(String subnetId, String handleId){

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        SubnetDhcpHandle subnetDhcpHandle  = null;
        boolean caRun = false;
        boolean operationSucc = true;

        try{
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            subnetDhcpHandle = dhcpHandlerDao.getById(subnetId);
            //已经被删
            if(subnetDhcpHandle == null){
                return null;
            }

            if(subnetDhcpHandle.getHandleId() == null || !subnetDhcpHandle.getHandleId().equals(handleId)){
                throw new GCloudException("::已经被其他处�?");
            }

            caRun = true;
        }catch (Exception ex){

        }

        try{
            PortDao portDao = SpringUtil.getBean(PortDao.class);
            List<Port> gcloudPorts = portDao.subnetPorts(subnetId, DeviceOwner.DHCP.getValue());
            if(gcloudPorts != null && !gcloudPorts.isEmpty()){
                IPortService portService = SpringUtil.getBean(IPortService.class);
                for(com.gcloud.controller.network.entity.Port port : gcloudPorts){
                    portService.cleanPortData(port.getId());
                }
            }
        }catch (Exception cex){
            operationSucc = false;
            log.error("subnet dhcp cleanDhcpPort error:", cex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        return new SubnetDhcpHandleInfo(caRun, subnetDhcpHandle, operationSucc);

    }


    public static SubnetDhcpHandleInfo deleteSubnet(String subnetId, String handleId){

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        SubnetDhcpHandle subnetDhcpHandle  = null;
        boolean caRun = false;
        boolean operationSucc = true;

        try{
            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            subnetDhcpHandle = dhcpHandlerDao.getById(subnetId);
            //已经被删
            if(subnetDhcpHandle == null){
                return null;
            }

            if(subnetDhcpHandle.getHandleId() == null || !subnetDhcpHandle.getHandleId().equals(handleId)){
                throw new GCloudException("::已经被其他处�?");
            }

            caRun = true;
        }catch (Exception ex){

        }

        try{
            PortDao portDao = SpringUtil.getBean(PortDao.class);
            List<Port> gcloudPorts = portDao.subnetPorts(subnetId, DeviceOwner.DHCP.getValue());
            if(gcloudPorts != null && !gcloudPorts.isEmpty()){
                IPortService portService = SpringUtil.getBean(IPortService.class);
                for(com.gcloud.controller.network.entity.Port port : gcloudPorts){
                    portService.cleanPortData(port.getId());
                }
            }

            SubnetDhcpHandleDao dhcpHandlerDao = SpringUtil.getBean(SubnetDhcpHandleDao.class);
            dhcpHandlerDao.deleteById(subnetId);

        }catch (Exception cex){
            operationSucc = false;
            log.error("subnet dhcp cleanDhcpPort error:", cex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        return new SubnetDhcpHandleInfo(caRun, subnetDhcpHandle, operationSucc);

    }

    //不要事务
    public static void createOrUpdateHandle(String subnetId, String neutronSubnetId){

        String handleId = UUID.randomUUID().toString();
        log.debug(String.format("create handle subnet [%s] handle [%s]", subnetId, handleId));

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        try{
            ISubnetDhcpHandleService subnetDhcpHandleService = SpringUtil.getBean(ISubnetDhcpHandleService.class);
            subnetDhcpHandleService.createOrUpdateHandle(subnetId, handleId);
            beginRun(subnetId);

        }catch (Exception ex){
            log.error("subnet dhcp createOrUpdate error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        SyncNeutronDhcpPortAsync async = new SyncNeutronDhcpPortAsync(subnetId, neutronSubnetId, handleId);
        async.start();
    }

    public static void deleteHandle(String subnetId, String neutronSubnetId){

        String handleId = UUID.randomUUID().toString();
        log.debug(String.format("create handle subnet [%s] handle [%s]", subnetId, handleId));

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        try{
            ISubnetDhcpHandleService subnetDhcpHandleService = SpringUtil.getBean(ISubnetDhcpHandleService.class);
            subnetDhcpHandleService.deleteHandle(subnetId, handleId);
            beginRun(subnetId);
        }catch (Exception ex){
            log.error("subnet dhcp createOrUpdate error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

        DeleteSubnetNeutronDhcpPortAsync async = new DeleteSubnetNeutronDhcpPortAsync(subnetId, neutronSubnetId, handleId);
        async.start();

    }

    public static void rollbackHandle(String subnetId){

        String handleId = UUID.randomUUID().toString();
        log.debug(String.format("create handle subnet [%s] handle [%s]", subnetId, handleId));

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        try{
            ISubnetDhcpHandleService subnetDhcpHandleService = SpringUtil.getBean(ISubnetDhcpHandleService.class);
            subnetDhcpHandleService.rollbackHandle(subnetId, handleId);
            beginRun(subnetId);
        }catch (Exception ex){
            log.error("subnet dhcp createOrUpdate error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);
    }

    public static void finishHandle(String subnetId, String handleId){

        String lockKey = NeutronSubnetDhcpUtil.dhcpLockKey(subnetId);
        String lockId = LockUtil.spinLock(lockKey, Consts.Time.DHCP_LOCK_TIMEOUT, Consts.Time.DHCP_LOCK_GET_TIMEOUT);

        try{
            ISubnetDhcpHandleService subnetDhcpHandleService = SpringUtil.getBean(ISubnetDhcpHandleService.class);
            subnetDhcpHandleService.finishHandle(subnetId, handleId);
            runningFinish(subnetId);
        }catch (Exception ex){
            log.error("subnet dhcp createOrUpdate error:", ex);
        }

        LockUtil.releaseSpinLock(lockKey, lockId);

    }

    public static void beginRun(String subnetId){
        GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
        String runningKey = NeutronSubnetDhcpUtil.dhcpRunningKey(subnetId);
        redisTemplate.opsForValue().set(runningKey, subnetId, Duration.ofMillis(Consts.Time.DHCP_RUNNING_TIMEOUT));
    }

    public static void runningFinish(String subnetId){
        GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
        String runningKey = NeutronSubnetDhcpUtil.dhcpRunningKey(subnetId);
        redisTemplate.delete(runningKey);
    }

    public static void running(String subnetId){
        GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
        String runningKey = NeutronSubnetDhcpUtil.dhcpRunningKey(subnetId);
        redisTemplate.expire(runningKey, Consts.Time.DHCP_RUNNING_TIMEOUT, TimeUnit.MILLISECONDS );
    }

}