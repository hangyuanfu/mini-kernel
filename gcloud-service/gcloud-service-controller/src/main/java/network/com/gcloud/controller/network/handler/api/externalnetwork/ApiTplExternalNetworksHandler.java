package com.gcloud.controller.network.handler.api.externalnetwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksMsg;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="TplExternalNetworks",name="网络拓扑-外网列表")
public class ApiTplExternalNetworksHandler extends MessageHandler<ApiTplExternalNetworksMsg, ApiTplExternalNetworksReplyMsg>{
	@Autowired
	INetworkService service;

	@Override
	public ApiTplExternalNetworksReplyMsg handle(ApiTplExternalNetworksMsg msg) throws GCloudException {
		ApiTplExternalNetworksReplyMsg reply = new ApiTplExternalNetworksReplyMsg();
		reply.setExternalNetworks(service.tplExternalNetworks(msg));
		return reply;
	}
}