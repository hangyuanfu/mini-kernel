package com.gcloud.controller.network.handler.api.subnet;

import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.CreateSubnetMsg;
import com.gcloud.header.network.msg.api.CreateSubnetReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@GcLog(taskExpect="创建子网")
@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="CreateSubnet",name="创建子网")
public class ApiCreateSubnetHandler  extends MessageHandler<CreateSubnetMsg, CreateSubnetReplyMsg> {
	@Autowired
	private ISubnetService subnetService;

	@Override
	public CreateSubnetReplyMsg handle(CreateSubnetMsg msg) throws GCloudException {
		CreateSubnetReplyMsg reply = new CreateSubnetReplyMsg();
		CreateSubnetParams params = new CreateSubnetParams();
		params.setCidrBlock(msg.getCidrBlock());
		params.setDnsNameServers(msg.getDnsNameServers());
		params.setGatewayIp(msg.getGatewayIp());
		params.setNetworkId(msg.getNetworkId());
		params.setSubnetName(msg.getSubnetName());
		params.setDhcp(msg.getDhcp());
		params.setAllocationPools(msg.getAllocationPools());
		reply.setSubnetId(subnetService.createSubnet(params, msg.getCurrentUser()));
		
		msg.setObjectId(reply.getSubnetId());
		msg.setObjectName(msg.getSubnetName());
		return reply;
	}

}