package com.gcloud.controller.security.handler.api.cluster;

import com.gcloud.controller.security.model.workflow.DisableSecurityClusterHaInitFlowCommandRes;
import com.gcloud.controller.security.workflow.cluster.DisableSecurityClusterHaWorkflow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.log.model.Task;
import com.gcloud.header.security.msg.api.cluster.ApiDisableSecurityClusterHaMsg;
import com.gcloud.header.security.msg.api.cluster.ApiDisableSecurityClusterHaReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "DisableSecurityClusterHa", name = "禁用安全集群的HA")
@LongTask
@GcLog(taskExpect = "禁用HA")
public class ApiDisableSecurityClusterHaHandler extends BaseWorkFlowHandler<ApiDisableSecurityClusterHaMsg, ApiDisableSecurityClusterHaReplyMsg> {

    @Override
    public Object preProcess(ApiDisableSecurityClusterHaMsg msg) throws GCloudException {
        return null;
    }

    @Override
    public ApiDisableSecurityClusterHaReplyMsg process(ApiDisableSecurityClusterHaMsg msg) throws GCloudException {

        ApiDisableSecurityClusterHaReplyMsg replyMessage = new ApiDisableSecurityClusterHaReplyMsg();
        DisableSecurityClusterHaInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DisableSecurityClusterHaInitFlowCommandRes.class);

        msg.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getClusterId()).expect("安全集群禁用HA成功").build());

        replyMessage.setTaskId(res.getTaskId());
        return replyMessage;

    }

    @Override
    public Class getWorkflowClass() {
        return DisableSecurityClusterHaWorkflow.class;
    }
}