package com.gcloud.controller.security.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityClusterComponentState {

    CREATING("创建�?", SecurityBaseState.RUNNING),
    CREATED("已创�?", SecurityBaseState.SUCCESS),
    DELETING("删除�?", SecurityBaseState.RUNNING),
    DELETED("已删�?", SecurityBaseState.SUCCESS),
    DELETE_FAILED("删除失败", SecurityBaseState.FAILED),
    FAIL("失败", SecurityBaseState.FAILED);

    private String cnName;
    private SecurityBaseState securityBaseState;

    SecurityClusterComponentState(String cnName, SecurityBaseState securityBaseState) {
        this.cnName = cnName;
        this.securityBaseState = securityBaseState;
    }

    public static Map<String, String> valueCnMap(){
        Map<String, String> result = new HashMap<>();
        Arrays.stream(SecurityClusterComponentState.values()).forEach(s -> result.put(s.value(), s.getCnName()));
        return result;
    }

    public static SecurityClusterComponentState getByValue(String value){
        return Arrays.stream(SecurityClusterComponentState.values()).filter(s -> s.value().equals(value)).findFirst().orElse(null);
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getCnName() {
        return cnName;
    }

    public SecurityBaseState getSecurityBaseState() {
        return securityBaseState;
    }
}