package com.gcloud.controller.compute.model.node;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NodeCommentInfo {
	
	HYPERVISOR("hypervisor"),
    KERNEL_VERSION("kernelVersion"),
    CPU_TYPE("cpuType"),
    CLOUD_PLATFORM("cloudPlatform"),
    IS_FT("isFt"),
    LXC_CPU_USED("cpuUsed"),
    LXC_MEMORY_USED("memoryUsed");
	
    private String value;
    
    NodeCommentInfo(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }
	
}