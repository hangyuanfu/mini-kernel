package com.gcloud.controller.compute.handler.api.vm.base;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.ApiStopInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiStopInstanceReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS,subModule=SubModule.VM, action = "StopInstance")
@LongTask
@GcLog(taskExpect = "关闭云服务器")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiStopInstanceHandler extends MessageHandler<ApiStopInstanceMsg, ApiStopInstanceReplyMsg> {

    @Autowired
    private IVmBaseService vmBaseService;

    @Override
    public ApiStopInstanceReplyMsg handle(ApiStopInstanceMsg msg) throws GCloudException {
        vmBaseService.stopInstance(msg.getInstanceId(), false, true, msg.getTaskId());
        ApiStopInstanceReplyMsg reply = new ApiStopInstanceReplyMsg();
        msg.setInstanceId(msg.getInstanceId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId()));

        return reply;
    }

}