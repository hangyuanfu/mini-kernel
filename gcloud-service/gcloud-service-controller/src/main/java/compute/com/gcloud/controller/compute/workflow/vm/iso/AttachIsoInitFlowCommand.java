package com.gcloud.controller.compute.workflow.vm.iso;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.compute.service.vm.iso.IVmIsoService;
import com.gcloud.controller.compute.workflow.model.iso.AttachIsoInitFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.iso.AttachIsoInitFlowCommandRes;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class AttachIsoInitFlowCommand extends BaseWorkFlowCommand{
	
	@Autowired
    private VolumeDao volumeDao;

    @Autowired
    private IVmBaseService vmBaseService;

    @Autowired
    private IVmIsoService vmIsoService;
    
    @Autowired
    private VolumeAttachmentDao volumeAttachmentDao;
	
	@Override
	protected Object process() throws Exception {
		//查询vm系统盘存储类型以及所在池
	    //修改vm state 为正在挂载光�?
	    //返回池ID
		
		AttachIsoInitFlowCommandReq req = (AttachIsoInitFlowCommandReq) getReqParams();
		vmIsoService.attachIsoInit(req.getInstanceId(), req.getIsoId(), req.isCreateVmTask()?req.getInTask():false);

		AttachIsoInitFlowCommandRes res = new AttachIsoInitFlowCommandRes();
		try {
			List<VolumeAttachment> volumeAttachs = volumeAttachmentDao.getInstanceRefAttach(req.getInstanceId());
			Optional<VolumeAttachment> systemVolumeAttachO = volumeAttachs.stream().filter(a -> "/dev/vda".equals(a.getMountpoint())).findFirst();
			if(systemVolumeAttachO.isPresent()) {
				VolumeAttachment systemVolumeAttach = systemVolumeAttachO.get();
				Volume systemVolume = volumeDao.getById(systemVolumeAttach.getVolumeId());
				res.setStoragePoolId(systemVolume.getPoolId());
				res.setHostname(systemVolumeAttach.getAttachedHost());
			}

			res.setTaskId(UUID.randomUUID().toString());
		} catch (Exception ex) {
			errorRollback();
			throw ex;
		}
		if(req.isCreateVmTask()) {
			VmInstance vm = vmBaseService.getInstanceById(req.getInstanceId());
			res.setPlatform(vm.getPlatform());
		}

		return res;
	}
	
	public void errorRollback() {
		AttachIsoInitFlowCommandReq req = (AttachIsoInitFlowCommandReq) getReqParams();

		vmBaseService.cleanState(req.getInstanceId(), req.getInTask());
	}

	@Override
	protected Object rollback() throws Exception {
		errorRollback();
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return AttachIsoInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return AttachIsoInitFlowCommandRes.class;
	}

}