package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.service.vm.senior.IVmSeniorService;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmInitFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateVmInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateDiskInfo;
import com.gcloud.controller.compute.workflow.model.senior.migrate.model.MigrateNetcardInfo;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.VmState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class MigrateVmInitFlowCommand extends BaseWorkFlowCommand{
	
	@Autowired
	private InstanceDao instanceDao;

	@Autowired
	private VolumeAttachmentDao volumeAttachmentDao;

	@Autowired
    private IVmSeniorService vmSeniorService;

	@Autowired
	private PortDao portDao;

	@Override
	protected Object process() throws Exception {
		log.debug("MigrateVmInitFlowCommand start");
		MigrateVmInitFlowCommandReq req = (MigrateVmInitFlowCommandReq) getReqParams();

		VmInstance instance = vmSeniorService.migrateInit(req.getInstanceId(), req.getTargetHostName());
        String instanceId = instance.getId();

		Map<String, Object> volumeAttachFilter = new HashMap<>();
		volumeAttachFilter.put(VolumeAttachment.INSTANCE_UUID, instance.getId());
		volumeAttachFilter.put(VolumeAttachment.ATTACHED_HOST, instance.getHostname());

		List<VolumeAttachment> volumeAttachments = volumeAttachmentDao.findByProperties(volumeAttachFilter);
		List<Port> ports = portDao.instancePorts(instanceId);

		MigrateVmInitFlowCommandRes res = new MigrateVmInitFlowCommandRes();

		if(volumeAttachments != null && volumeAttachments.size() > 0){
			List<MigrateDiskInfo> diskInfos = volumeAttachments.stream().map(va -> new MigrateDiskInfo(va.getVolumeId())).collect(Collectors.toList());
			res.setDisks(diskInfos);
		}

		if(ports != null && ports.size() > 0){
			List<MigrateNetcardInfo> portInfos = ports.stream().map(p -> new MigrateNetcardInfo(p.getId())).collect(Collectors.toList());
			res.setNetcards(portInfos);
		}

		res.setInstanceId(instanceId);
		res.setSourceHostName(instance.getHostname());
		res.setBeginStatus(instance.getState());
		res.setTargetHostName(req.getTargetHostName());
		res.setImageId(instance.getImageId());

		log.debug("MigrateVmInitFlowCommand end");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		MigrateVmInitFlowCommandRes res = (MigrateVmInitFlowCommandRes)getResParams();
		MigrateVmInitFlowCommandReq req = (MigrateVmInitFlowCommandReq)getReqParams();

		VmInstance instance = instanceDao.getById(res.getInstanceId());
		if(instance == null){
			log.error("迁移回滚，云服务器不存在，instanceId = " + res.getInstanceId());
			return null;
		}

		//如果�?�?始是�?机，则会占用了目标节点资源，�?要释放资�?
		if(VmState.RUNNING.value().equalsIgnoreCase(res.getBeginStatus())){
			Dispatcher.dispatcher().release(req.getTargetHostName(), instance.getCore(), instance.getMemory());
		}

		List<String> updateFiled = new ArrayList<>();

		instance.setStepState(null);
		updateFiled.add(VmInstance.STEP_STATE);
		if(req.getInTask() == null || !req.getInTask()){
			instance.setTaskState(null);
			updateFiled.add(VmInstance.TASK_STATE);
		}
		instance.setMigrateTo(null);
		updateFiled.add(VmInstance.MIGRATE_TO);

		instanceDao.update(instance, updateFiled);


		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return MigrateVmInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return MigrateVmInitFlowCommandRes.class;
	}

}