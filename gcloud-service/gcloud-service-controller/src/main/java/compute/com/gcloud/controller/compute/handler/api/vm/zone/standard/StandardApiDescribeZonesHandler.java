package com.gcloud.controller.compute.handler.api.vm.zone.standard;

import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.model.standard.StandardZoneType;
import com.gcloud.header.compute.msg.api.vm.zone.standard.StandardApiDescribeZonesMsg;
import com.gcloud.header.compute.msg.api.vm.zone.standard.StandardApiDescribeZonesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "DescribeZones", versions = {ApiVersion.Standard}, name = "可用区列�?")
public class StandardApiDescribeZonesHandler extends MessageHandler<StandardApiDescribeZonesMsg, StandardApiDescribeZonesReplyMsg>{

	@Autowired
    private IVmZoneService zoneService;
	
	@Override
	public StandardApiDescribeZonesReplyMsg handle(StandardApiDescribeZonesMsg msg) throws GCloudException {
		StandardApiDescribeZonesReplyMsg reply = new StandardApiDescribeZonesReplyMsg();
		DescribeZonesParams params = new DescribeZonesParams();
		params.setPageNumber(msg.getPageNumber());
		params.setPageSize(msg.getPageSize());
		PageResult<AvailableZone> response = zoneService.describeZones(params);
		PageResult<StandardZoneType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
		
		reply.init(stdResponse);
		return reply;
	}
	
	public List<StandardZoneType> toStandardReply(List<AvailableZone> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardZoneType> stdList = new ArrayList<>();
		for (AvailableZone data : list) {
			StandardZoneType stdData = BeanUtil.copyProperties(data, StandardZoneType.class);
			stdList.add(stdData);
		}
		return stdList;
	}

}