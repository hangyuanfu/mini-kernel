package com.gcloud.controller.compute.handler.api.vm.storage;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.storage.IVmDiskService;
import com.gcloud.controller.compute.workflow.model.storage.DetachDataDiskInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.storage.DetachDataDiskWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.storage.DetachDataDiskWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.storage.ApiDetachDiskMsg;
import com.gcloud.header.compute.msg.api.vm.storage.ApiDetachDiskReplyMsg;
import com.gcloud.header.log.model.Task;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(isMultiLog = true, taskExpect = "卸载磁盘")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = "DetachDisk")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.VOLUME, resourceIdField = "diskId")
public class ApiDetachDiskHandler extends BaseWorkFlowHandler<ApiDetachDiskMsg, ApiDetachDiskReplyMsg> {

	@Autowired
	private IVmDiskService diskService;

	@Override
	public ApiDetachDiskReplyMsg process(ApiDetachDiskMsg msg) throws GCloudException {
		ApiDetachDiskReplyMsg reply = new ApiDetachDiskReplyMsg();
		// 记录操作日志，任务流无论单操作还是批�? 都用这种方式记录操作日志
		DetachDataDiskInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DetachDataDiskInitFlowCommandRes.class);
		
		//String except = String.format("卸载磁盘[%s]成功", res.getVolumeId());
		String except = String.format("卸载磁盘[%s]成功", CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, res.getVolumeId()));
		reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getVolumeId()).objectName(res.getVolumeName()).expect(except).build());

		String requestId = UUID.randomUUID().toString();
		reply.setRequestId(requestId);
		reply.setSuccess(true);
		return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return DetachDataDiskWorkflow.class;
	}

	@Override
	public Object preProcess(ApiDetachDiskMsg msg) throws GCloudException {
		diskService.detachDataDiskApiCheck(msg.getDiskId());
		return null;
	}

	@Override
	public Object initParams(ApiDetachDiskMsg msg) {
        DetachDataDiskWorkflowReq req = new DetachDataDiskWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setVolumeId(msg.getDiskId());
        req.setInTask(false);
		return req;
	}
}