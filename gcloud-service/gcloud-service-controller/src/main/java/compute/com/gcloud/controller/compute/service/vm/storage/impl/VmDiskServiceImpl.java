package com.gcloud.controller.compute.service.vm.storage.impl;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.service.vm.storage.IVmDiskService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.enums.VmTaskState;
import com.gcloud.header.storage.enums.DiskType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Slf4j
@Transactional
public class VmDiskServiceImpl implements IVmDiskService {

	@Autowired
	private InstanceDao instanceDao;

	@Autowired
	private VolumeDao volumeDao;

	@Autowired
	private VolumeAttachmentDao volumeAttachmentDao;

	@Autowired
    private IVolumeService volumeService;
	
	@Autowired
	private StoragePoolDao storagePoolDao;

	public void attachDataDiskInit(String instanceId, String volumeId, boolean inTask) {
		VmInstance vm = instanceDao.getById(instanceId);
		if (vm == null) {
			throw new GCloudException("0010903::云服务器不存�?");
		}

		Volume volume = volumeDao.getById(volumeId);
		if (volume == null) {
			throw new GCloudException("0010904::磁盘不存�?");
		}

		//暂时不允许多挂载
		if(!volume.getStatus().equals(org.openstack4j.model.storage.block.Volume.Status.AVAILABLE.value())){
			throw new GCloudException("0010905::磁盘当前状�?�不能挂�?");
		}

		Node node = RedisNodesUtil.getComputeNodeByHostName(vm.getHostname());
		if (node == null) {
			throw new GCloudException("0010906::找不到云服务�?");
		}

		if (!inTask) {
			if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.ATTACH_DISK)) {
				throw new GCloudException("0010907::云服务器当前无法挂载磁盘");
			}
		}
		
		if(volume.getStorageType().equals(StorageType.LOCAL.getValue())) {
			StoragePool pool = storagePoolDao.getById(volume.getPoolId());
			if(!pool.getHostname().equals(vm.getHostname())) {
				throw new GCloudException("0010908::云服务器无法挂载不同节点的本地磁�?");
			}
		}

		//保留volume，用于挂�?
        volumeService.reserveVolume(volumeId);

	}

	@Override
	public void detachDataDiskInit(String instanceId, String volumeId, boolean inTask) {

		VmInstance vm = instanceDao.getById(instanceId);
		if (vm == null) {
			throw new GCloudException("0011003::云服务器不存�?");
		}

		Volume volume = volumeDao.getById(volumeId);
		if (volume == null) {
			throw new GCloudException("0011004::磁盘不存�?");
		}

//		if(DiskType.SYSTEM.getValue().equals(volume.getDiskType())){
//			throw new GCloudException("0011008::系统盘不能卸�?");
//		}

		List<VolumeAttachment> volumeAttachments = volumeAttachmentDao.findByVolumeIdAndInstanceId(volumeId, instanceId);
		if (volumeAttachments == null || volumeAttachments.size() == 0) {
			throw new GCloudException("0011005::云服务器没有挂载此磁�?");
		}

		Node node = RedisNodesUtil.getComputeNodeByHostName(vm.getHostname());
		if (node == null) {
			throw new GCloudException("0011006::找不到云服务�?");
		}

		if (!inTask) {
			if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.DETACH_DISK)) {
				throw new GCloudException("0011007::云服务器当前状�?�不能卸载云�?");
			}
		}

		volumeService.beginDetachingVolume(volumeId);
	}

	@Override
	public void detachDataDiskApiCheck(String volumeId) {
		Volume volume = volumeDao.getById(volumeId);
		if (volume == null) {
			throw new GCloudException("0011009::磁盘不存�?");
		}

		if(DiskType.SYSTEM.getValue().equals(volume.getDiskType())){
			throw new GCloudException("0011008::系统盘不能卸�?");
		}
	}
}