package com.gcloud.controller.compute.handler.api.vm.zone;

import com.gcloud.controller.compute.model.vm.CreateZoneParams;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.zone.ApiCreateZoneMsg;
import com.gcloud.header.compute.msg.api.vm.zone.ApiCreateZoneReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "CreateZone", name = "新增可用�?")
public class ApiCreateZoneHandler extends MessageHandler<ApiCreateZoneMsg, ApiCreateZoneReplyMsg> {

    @Autowired
    private IVmZoneService zoneService;

    @Override
    public ApiCreateZoneReplyMsg handle(ApiCreateZoneMsg msg) throws GCloudException {
    	CreateZoneParams params = BeanUtil.copyProperties(msg, CreateZoneParams.class);
    	String zoneId = this.zoneService.createZone(params);
    	
        ApiCreateZoneReplyMsg replyMessage = new ApiCreateZoneReplyMsg();
        replyMessage.setZoneId(zoneId);
        return replyMessage;
    }

}