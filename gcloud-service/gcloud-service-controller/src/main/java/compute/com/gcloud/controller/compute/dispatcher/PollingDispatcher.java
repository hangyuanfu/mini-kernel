package com.gcloud.controller.compute.dispatcher;

import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.core.exception.GCloudException;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.DependsOn;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class PollingDispatcher extends Dispatcher {


    @Override
    public Node assignNode(Integer core, Integer memory) {
        Map<String, Node> nodes = RedisNodesUtil.getComputeNodes();
        if(nodes == null || nodes.size() == 0){
            throw new GCloudException("::没有合�?�的节点");
        }

        List<String> hostnames = new ArrayList<>();
        hostnames.addAll(nodes.keySet());
        Collections.sort(hostnames);

//        LockUtil.spinLock(Consts.RedisKey.GCLOUD_CONTROLLER_COMPUTE_DISPATCHER_LOCK, )

        return null;
    }

    @Override
    public Node assignNodeInZone(String zoneId, Integer core, Integer memory) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void assignNode(String hostname, Integer core, Integer memory) {

    }

    @Override
    public Node assignNode(List<String> hostname, Integer core, Integer memory) {
        return null;
    }
}