package com.gcloud.controller.compute.workflow.model.vm;

import com.gcloud.header.compute.msg.api.model.DiskInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CheckAndDistributeImageFlowCommandReq {
	private String imageId;
	private String zoneId;
	private String systemDiskCategory;//Category id
	private String createHost;
	private DiskInfo systemDisk;

	public DiskInfo getSystemDisk() {
		return systemDisk;
	}

	public void setSystemDisk(DiskInfo systemDisk) {
		this.systemDisk = systemDisk;
	}

	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getSystemDiskCategory() {
		return systemDiskCategory;
	}
	public void setSystemDiskCategory(String systemDiskCategory) {
		this.systemDiskCategory = systemDiskCategory;
	}
	public String getCreateHost() {
		return createHost;
	}
	public void setCreateHost(String createHost) {
		this.createHost = createHost;
	}
	
	
}