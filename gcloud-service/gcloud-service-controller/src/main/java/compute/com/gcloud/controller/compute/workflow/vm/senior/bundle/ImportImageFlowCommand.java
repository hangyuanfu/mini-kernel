package com.gcloud.controller.compute.workflow.vm.senior.bundle;

import com.gcloud.controller.compute.workflow.model.senior.ImportImageFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.ImportImageFlowCommandRes;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Scope("prototype")
@Slf4j
public class ImportImageFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private IImageService imageService;

    @Override
    protected Object process() throws Exception {

        ImportImageFlowCommandReq req = (ImportImageFlowCommandReq)this.getReqParams();

        CreateImageParams params = new CreateImageParams();
        params.setTaskId(this.getTaskId());
        params.setImageName(req.getImageName());
        params.setArchitecture(req.getArchitecture());
        params.setFilePath(req.getFilePath());
        params.setDescription(req.getDescription());
        params.setOsType(req.getOsType());
        params.setImageId(req.getImageId());

        String imageId = imageService.createImage(params, req.getCurrentUser());
        ImportImageFlowCommandRes res = new ImportImageFlowCommandRes();
        res.setImageId(imageId);

        return res;
    }

    @Override
    protected Object rollback() throws Exception {
        ImportImageFlowCommandRes res = (ImportImageFlowCommandRes)getResParams();
        imageService.deleteImage(res.getImageId());
        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return ImportImageFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return ImportImageFlowCommandRes.class;
    }

    @Override
    public int getTimeOut() {
        //�?小时
        return 3600;
    }
}