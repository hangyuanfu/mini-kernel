package com.gcloud.controller.compute.cache;

import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.service.common.Consts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
public class CacheRedis {

    @Autowired
    private GCloudRedisTemplate redisTemplate;

    public void delUnAdoptObject(String hostName, String id) throws GCloudException {
        String setName = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_COMPUTE_ADOPT_SET, hostName);
        if (id != null) {
            redisTemplate.opsForHash().delete(setName, id);
        } else {
            redisTemplate.delete(setName);
        }
    }

}