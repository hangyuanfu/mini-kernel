package com.gcloud.controller.compute.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Jdbc("controllerJdbcTemplate")
@Repository
public class ZoneDao extends JdbcBaseDaoImpl<AvailableZoneEntity, String> {

    public PageResult<AvailableZoneEntity> page(DescribeZonesParams params){
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	sql.append("select * from gc_zones t where 1 = 1 ");
        
        if(params.getEnabled() != null) {
        	sql.append(" and t.enabled = ?");
        	values.add(params.getEnabled().booleanValue());
        }

        if(StringUtils.isNotBlank(params.getDiskCategoryId())){
        	sql.append(" and t.id in (select d.zone_id from gc_zone_disk_categories d where d.disk_category_id = ?)");
        	values.add(params.getDiskCategoryId());
		}

        if(StringUtils.isNotBlank(params.getPoolId())){
            sql.append(" and t.id in (select z.zone_id from gc_storage_pool_zones z where z.storage_pool_id = ?)");
            values.add(params.getPoolId());
        }

        return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize());

    }
    
    public List<AvailableZoneEntity> findByIds(List<String> ids) {
    	if(null == ids || ids.isEmpty()) {
    		return null;
    	}
    	
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select * from gc_zones where id in (").append(SqlUtil.inPreStr(ids.size())).append(")");
    	values.addAll(ids);
    	
    	return findBySql(sql.toString(), values);
    }
    
    public List<String> findNamesByIds(List<String> ids) {
    	if(null == ids || ids.isEmpty()) {
    		return null;
    	}
    	
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select name from gc_zones where id in (").append(SqlUtil.inPreStr(ids.size())).append(")");
    	values.addAll(ids);
    	List<AvailableZoneEntity> list = findBySql(sql.toString(), values);
    	if(list == null || list.isEmpty()) {
    		return null;
    	}
    	
    	List<String> names = new ArrayList<>();
    	for (AvailableZoneEntity item : list) {
    		names.add(item.getName());
		}
    	
    	return names;
    }
    
    public <E> PageResult<E> findPageByHostname(String hostname, Integer pageNumber, Integer pageSize, Class<E> clazz) {
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select * from gc_zones where id in ");
    	sql.append(" (");
    	sql.append(" select zone_id from gc_compute_nodes where hostname = ?");
    	sql.append(" )");
    	
    	values.add(hostname);
    	
    	return findBySql(sql.toString(), values, pageNumber, pageSize, clazz);
    }
}