package com.gcloud.controller.log.util;

import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.controller.log.service.ILogService;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.log.enums.LogStatus;
import com.gcloud.header.log.enums.LogType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LongTaskUtil {

    public static void syncSucc(LogType logType, String taskId){
        syncSucc(logType, taskId, null);
    }

    public static void syncSucc(LogType logType, String taskId, String objectId){
        if(logType == null || !logType.equals(LogType.SYNC)){
            return;
        }

        ILogService logService = SpringUtil.getBean(ILogService.class);
        LogFeedbackParams param = new LogFeedbackParams();
        param.setObjectId(objectId);
        param.setStatus(LogStatus.COMPLETE.getValue());
        param.setTaskId(taskId);
        logService.feedbackAsync(param);

    }
    
    public static void syncFail(LogType logType, String taskId, String objectId){
        if(logType == null || !logType.equals(LogType.SYNC)){
            return;
        }

        ILogService logService = SpringUtil.getBean(ILogService.class);
        LogFeedbackParams param = new LogFeedbackParams();
        param.setObjectId(objectId);
        param.setStatus(LogStatus.FAILED.getValue());
        param.setTaskId(taskId);
        logService.feedbackAsync(param);

    }

}