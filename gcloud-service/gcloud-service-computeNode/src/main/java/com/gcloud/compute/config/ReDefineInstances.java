package com.gcloud.compute.config;

import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.util.ConfigFileUtil;
import com.gcloud.compute.util.VmNodeUtil;
import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.service.common.compute.model.DomainListInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
@ConditionalOnExpression("${gcloud.computeNode.redefineOnBoot:false} == true")
public class ReDefineInstances {

    @Autowired
    private IVmVirtual vmVirtual;

    @Autowired
    private ComputeNodeProp computeNodeProp;

    @PostConstruct
    public void redefine(){

        List<DomainListInfo> domainListInfos = vmVirtual.listVm(computeNodeProp.getNodeIp(), true, false);
        if(domainListInfos == null || domainListInfos.size() == 0){
            return;
        }

        for(DomainListInfo listInfo : domainListInfos){
            String libvirtPath = VmNodeUtil.getLibvirtXmlPath(computeNodeProp.getNodeIp(), listInfo.getDomainAlias());
            File file = new File(libvirtPath);
            //文件不存�?
            if(!file.exists()){
                log.error("配置文件不存�?: " + libvirtPath);
                continue;
            }
            try{
                vmVirtual.define(libvirtPath);
            }catch (Exception ex){
                log.error("define 失败�?" + libvirtPath);
            }
        }
    }

}