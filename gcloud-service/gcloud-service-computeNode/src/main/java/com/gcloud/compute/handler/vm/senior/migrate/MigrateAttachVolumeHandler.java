package com.gcloud.compute.handler.vm.senior.migrate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.compute.service.vm.storage.IVmVolumeNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateAttachVolumeMsg;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateAttachVolumeReplyMsg;
import com.gcloud.header.storage.enums.DiskType;
import com.gcloud.header.storage.model.VmVolumeDetail;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class MigrateAttachVolumeHandler extends AsyncMessageHandler<MigrateAttachVolumeMsg>{

	@Autowired
    private MessageBus bus;
	
	@Autowired
	private IVmVolumeNodeService volumeNodeService;
	
	@Override
	public void handle(MigrateAttachVolumeMsg msg) {
		MigrateAttachVolumeReplyMsg replyMsg = msg.deriveMsg(MigrateAttachVolumeReplyMsg.class);
		boolean flag = true;
		
		String instanceId = msg.getInstanceId();
		List<VmVolumeDetail> volumeDetailList = msg.getVolumeDetailList();
		if(null == volumeDetailList) {
			log.error("MigrateAttachVolumeHandler未接受到volumeDetailList参数");
			flag = false;
			replyMsg.setErrorCode("MigrateAttachVolumeHandler未接受到volumeDetailList参数");
		} else {
			for (VmVolumeDetail volumeDetail : volumeDetailList) {
				try{
					if(!DiskType.SYSTEM.getValue().equals(volumeDetail.getDiskType())) {
						volumeNodeService.configDataDiskFile(instanceId, volumeDetail);
					}
				} catch(Exception e) {
					log.error("配置目标节点磁盘环境异常");
					flag = false;
					replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(e, "::配置目标节点磁盘环境异常"));
					continue;
				}
			}
		}
		
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		replyMsg.setSuccess(flag);
		replyMsg.setTaskId(msg.getTaskId());
		bus.send(replyMsg);
	}

}