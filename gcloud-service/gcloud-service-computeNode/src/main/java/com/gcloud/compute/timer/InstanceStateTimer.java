package com.gcloud.compute.timer;

import com.gcloud.compute.service.vm.base.IVmAdoptNodeService;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.node.model.ComputeNodeInfo;
import com.gcloud.header.compute.msg.node.vm.base.SyncStateMsg;
import com.gcloud.header.compute.msg.node.vm.model.VmStateInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Slf4j
public class InstanceStateTimer {

    @Autowired
    private IVmAdoptNodeService vmAdoptNodeService;

    @Autowired
    private MessageBus bus;

    @Autowired
    private ComputeNodeInfo nodeInfo;

    @Scheduled(fixedDelay = 10000L)
    public void refresh(){

        Map<String, VmStateInfo> stateInfos = vmAdoptNodeService.stateInfo();
        SyncStateMsg msg = new SyncStateMsg();
        msg.setServiceId(MessageUtil.controllerServiceId());
        msg.setStateInfos(stateInfos);
        msg.setHostname(nodeInfo.getHostname());

        bus.send(msg);

    }

}