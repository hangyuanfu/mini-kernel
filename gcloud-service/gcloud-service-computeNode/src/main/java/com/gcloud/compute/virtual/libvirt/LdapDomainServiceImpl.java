package com.gcloud.compute.virtual.libvirt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.compute.virtual.ILdapDomainService;
import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.header.compute.enums.PlatformType;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
@Slf4j
public class LdapDomainServiceImpl implements ILdapDomainService {
	static final int MAX_RESTART_OUNT = 5;
	@Autowired
	IVmVirtual vmVirtual;
	public boolean join(String instanceId,String domain,String user,String password) {
		String[] versionCmd = new String[] { "hostname" };
		String computerName = vmVirtual.agentGuestExecReturn(instanceId, versionCmd, 30, PlatformType.WINDOWS);
		
		int count = MAX_RESTART_OUNT;
		do {
			boolean ret = false;
			// 入域逻辑
			try {
				String[] cmd = new String[] { "cmd.exe", "/c", "netdom", "join", computerName == null ? "%computername%" : computerName, "/domain:" +domain, "/userd:" + user, "/passwordd:" + password, "/reboot", "20" };
				ret = vmVirtual.agentGuestExec(instanceId, cmd, 30);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			if (ret) {
				return true;
			}
			System.out.println(String.format("【云服务器入域�?? �?%s次结�?:result:%s", ((MAX_RESTART_OUNT - count) + 1), "ret = " + ret));
			try {
				Thread.sleep(((MAX_RESTART_OUNT - count) + 1) * 5 * 1000);
			} catch (Exception e) {
				System.out.println("【云服务器入域�?�线程错误：" + e.getMessage());
			}
			count--;
		}while(count>0);
		return false;
	}
	
	public boolean remove(String instanceId,String domain,String user,String password) {
		String[] versionCmd = new String[] { "hostname" };
		String computerName = vmVirtual.agentGuestExecReturn(instanceId, versionCmd, 30, PlatformType.WINDOWS);
		boolean ret = false;
		try {
			String[] cmd = new String[] { "cmd.exe", "/c", "netdom", "remove", computerName == null ? "%computername%" : computerName, "/domain:" + domain, "/userd:" + user, "/passwordd:" + password };
			ret = vmVirtual.agentGuestExec(instanceId, cmd, 30);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ret;
	}
	
	public boolean move(String instanceId,String sourceDomain,String sourceUser,String sourcePassword,String targetDomain,String targetUser,String targetPassword) {
		remove(instanceId, sourceDomain, sourceUser, sourcePassword);
		return join(instanceId,targetDomain,targetUser,targetPassword);
	}
}