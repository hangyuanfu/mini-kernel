package com.gcloud.compute.handler.vm.base;

import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.service.vm.base.IVmBaseNodeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.compute.msg.node.vm.base.QueryInstanceVNCMsg;
import com.gcloud.header.compute.msg.node.vm.base.QueryInstanceVNCMsgReply;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class QueryInstanceVNCHandler extends MessageHandler<QueryInstanceVNCMsg, QueryInstanceVNCMsgReply> {

    @Autowired
    private IVmBaseNodeService vmBaseNodeService;

    @Autowired
    private ComputeNodeProp prop;

    @Override
    public QueryInstanceVNCMsgReply handle(QueryInstanceVNCMsg msg) throws GCloudException {
        QueryInstanceVNCMsgReply reply = new QueryInstanceVNCMsgReply();
        String port = vmBaseNodeService.queryVnc(msg.getInstanceId());

        reply.setSuccess(true);
        reply.setPort(port);
        reply.setHostIp(prop.getNodeIp());
        return reply;
    }
}