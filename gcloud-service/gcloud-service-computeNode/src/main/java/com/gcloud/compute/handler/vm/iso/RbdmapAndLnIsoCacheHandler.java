package com.gcloud.compute.handler.vm.iso;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.FileUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.iso.RbdmapAndLnIsoCacheMsg;
import com.gcloud.header.compute.msg.node.vm.iso.RbdmapAndLnIsoCacheReplyMsg;
import com.gcloud.header.image.msg.node.CheckImageStoreMsg;
import com.gcloud.header.image.msg.node.CheckImageStoreReplyMsg;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.service.common.compute.uitls.VmUtil;
import com.gcloud.service.common.enums.ImageStoreStatus;
import com.gcloud.service.common.enums.RbdMapAction;
//import com.gcloud.header.storage.enums.StoragePoolDriver;
//import com.gcloud.service.common.compute.uitls.DiskUtil;
//import com.gcloud.service.common.compute.uitls.VmUtil;
import com.gcloud.service.common.util.RbdUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class RbdmapAndLnIsoCacheHandler extends AsyncMessageHandler<RbdmapAndLnIsoCacheMsg> {
	@Autowired
	private MessageBus bus;

	@Override
	public void handle(RbdmapAndLnIsoCacheMsg msg) {
		RbdmapAndLnIsoCacheReplyMsg replyMsg = msg.deriveMsg(RbdmapAndLnIsoCacheReplyMsg.class);
		replyMsg.setSuccess(false);
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		replyMsg.setHostname(msg.getHostname());
		replyMsg.setIsoId(msg.getIsoId());
		replyMsg.setPoolName(msg.getPoolName());
		try {
			if(msg.getRbdMapAction().equals(RbdMapAction.MAP.value())) {
				RbdUtil.rbdmap(msg.getIsoId(), msg.getPoolName(), "::rbd map失败");
			}else {
				checkRbdMap(msg);
			}
			/* 不用软连接，直接用map后的设备路径
			 * replyMsg.setMapPath(sourcePath);
			String destPath = VmUtil.getIsoPath(msg.getIsoId(), msg.getPoolName(), StoragePoolDriver.RBD.name());
			DiskUtil.mkdir(destPath.substring(0, destPath.lastIndexOf("/") +1));
			DiskUtil.ln(sourcePath, destPath);*/
			replyMsg.setSuccess(true);
		} catch (Exception ex) {
			log.error("rbd map 失败", ex);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::rbd map 失败"));
		}
		bus.send(replyMsg);
		
	}
	
	private void checkRbdMap(RbdmapAndLnIsoCacheMsg msg) {
		int times = 20;
		long sleepTime = 10000;
		for (int i = 1; i <= times; i++) {
			try {
				// �?�?
				log.debug(String.format("rbdmap poolName:%s,isoId:%s,hostname:%s�?%s次rbdmap�?测开�?", msg.getPoolName(), msg.getIsoId(), msg.getHostname(), i));
				String path = VmUtil.getIsoPath(msg.getIsoId(), msg.getPoolName(), StoragePoolDriver.RBD.name());
				File file = new File(path);
		        if (file.exists()) {
		        	log.debug("rbdmap�?测成�?");
		        	return ;
		        }
				log.debug(String.format("rbdmap poolName:%s,isoId:%s,hostname:%s�?%s次rbdmap�?测结�?", msg.getPoolName(), msg.getIsoId(), msg.getHostname(), i));
			} catch (Exception exx) {
				log.error("rbdmap�?测失�?", exx);
				log.debug(String.format("rbdmap poolName:%s,isoId:%s,hostname:%s�?%s次rbdmap�?测失�?", msg.getPoolName(), msg.getIsoId(), msg.getHostname(), i));
				if (i == times) {
					if (exx instanceof GCloudException) {
						throw (GCloudException) exx;
					} else {
						throw new GCloudException(String.format("rbdmap poolName:%s,isoId:%s,hostname:%s rbdmap�?测失�?", msg.getPoolName(), msg.getIsoId(), msg.getHostname()));
					}

				}
			}
			if (i < times) {
				try {
					Thread.sleep(sleepTime);
				} catch (Exception exb) {
					log.error(String.format("rbdmap poolName:%s,isoId:%s,hostname:%s rbdmap�?测失�?", msg.getPoolName(), msg.getIsoId(), msg.getHostname()), exb);
				}
			}
		}
	}

}