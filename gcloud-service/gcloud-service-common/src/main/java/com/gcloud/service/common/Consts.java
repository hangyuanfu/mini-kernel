package com.gcloud.service.common;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class Consts {

	public static final class RedisKey {

		private static final String GCLOUD_PREFIX = "gcloud";
		private static final String CONTROLLER_PREFIX = "controller";
		private static final String COMPUTE_PREFIX = "compute";
		private static final String STORAGE_PREFIX = "sorage";
		private static final String NETWORK_PREFIX = "network";

		private static final String CONTROLLER_COMPUTE_PREFIX = String.format("%s:%s:%s:", GCLOUD_PREFIX, CONTROLLER_PREFIX, COMPUTE_PREFIX);
		
		private static final String CONTROLLER_STORAGE_PREFIX = String.format("%s:%s:%s:", GCLOUD_PREFIX, CONTROLLER_PREFIX, STORAGE_PREFIX);

		private static final String CONTROLLER_NETWORK_PREFIX = String.format("%s:%s:%s:", GCLOUD_PREFIX, CONTROLLER_PREFIX, NETWORK_PREFIX);

		// 修改时，要修改对于的VmControllerUtil.getHostNameByAliveKey
		public static final String GCLOUD_CONTROLLER_COMPUTE_NODES_COMPUTE_NODE_ALIVE = CONTROLLER_COMPUTE_PREFIX + "nodes:compute_node_alive_{0}";
		// 修改时，要修改对于的VmControllerUtil.getHostNameByHostKey
		public static final String GCLOUD_CONTROLLER_COMPUTE_NODES_LOCK_HOSTBANE = CONTROLLER_COMPUTE_PREFIX + "nodes:lock_hostname_{0}";
		public static final String GCLOUD_CONTROLLER_COMPUTE_NODES_COMPUTE_NODE_HOSTNAME = CONTROLLER_COMPUTE_PREFIX + "nodes:compute_node_hostname_{0}";


		// node_ha
		public static final String GCLOUD_CONTROLLER_COMPUTE_NODE_HA_CACHE = CONTROLLER_COMPUTE_PREFIX + "ha:node_ha_{0}";

		// adopt
		public static final String GCLOUD_CONTROLLER_COMPUTE_ADOPT_SET = CONTROLLER_COMPUTE_PREFIX + "adopt:set_{0}";

		//sync
		//{0} 为hostname
		public static final String GCLOUD_CONTROLLER_COMPUTE_SYNC_INSTANCE_STATE = CONTROLLER_COMPUTE_PREFIX + "sync:instance_{0}";


		//dispatcher
		public static final String GCLOUD_CONTROLLER_COMPUTE_DISPATCHER_LOCK = CONTROLLER_COMPUTE_PREFIX + "dispatcher:dispatch";
		
		public static final String GCLOUD_CONTROLLER_STORAGE_NODE_ALIVE = CONTROLLER_STORAGE_PREFIX + "nodes:storage_node_alive_{0}";
		
		public static final String GCLOUD_CONTROLLER_STORAGE_NODE_LOCK = CONTROLLER_STORAGE_PREFIX + "nodes:storage_node_lock_{0}";
		
		public static final String GCLOUD_CONTROLLER_STORAGE_NODES_STORAGE_NODE_HOSTNAME = CONTROLLER_STORAGE_PREFIX + "nodes:storage_node_hostname_{0}";
		
		public static final String GCLOUD_CONTROLLER_STORAGE_SNAP_LOCK = CONTROLLER_STORAGE_PREFIX + ":storage_snap_lock_{0}_{1}";
		
		public static final String GCLOUD_CONTROLLER_STORAGE_POOL_INFO_COLLECT_CRON_LOCK = CONTROLLER_STORAGE_PREFIX + ":storage_pool_info_collect_cron_lock_{0}";



		//network
		public static final String GCLOUD_CONTROLLER_NETWORK_DHCP_LOCK = CONTROLLER_NETWORK_PREFIX + ":dhcp:{0}";
		public static final String GCLOUD_CONTROLLER_NETWORK_DHCP_RUNNING = CONTROLLER_NETWORK_PREFIX + ":dhcp_running:{0}";
	}

	public static final class Time {

		public static final long NODES_REDIS_LOCK_TIMEOUT = 1000L; // 单位ms
		public static final long NODES_REDIS_LOCK_GET_LOCK_TIMEOUT = -1 * 1000L; // 单位ms

        public static final long DISPATCHER_REDIS_LOCK_TIMEOUT = 1;
        public static final long DISPATCHER_REDIS_LOCK_GET_LOCK_TIMEOUT = -1 * 1000L;

        public static final long STORAGE_REDIS_SNAP_LOCK_TIMEOUT = 15000L; // 单位ms
		public static final long STORAGE_REDIS_SNAP_LOCK_GET_LOCK_TIMEOUT = -1 * 1000L; // 单位ms
		
		public static final long STORAGE_REDIS_POOL_INFO_COLLECT_CRON_LOCK_TIMEOUT = 15000L; // 单位ms
		public static final long STORAGE_REDIS_POOL_INFO_COLLECT_CRON_LOCK_GET_LOCK_TIMEOUT = -1 * 1000L; // 单位ms

		public static final long DHCP_LOCK_TIMEOUT = 10000L; //10s 超时
		public static final long DHCP_LOCK_GET_TIMEOUT = 15000L; //15s 超时

		public static final long DHCP_RUNNING_TIMEOUT = 600000L;//10分钟;
	}

	public static final class Hypervisor{

		public static final String HYPERVISOR_CONFIG = "gcloud.computeNode.hypervisor";
		public static final String KVM = "kvm";

	}

	public static final String DEFAULT_DISK_DRIVER = "virtio";

}