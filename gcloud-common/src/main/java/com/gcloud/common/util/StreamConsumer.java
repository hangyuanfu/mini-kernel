package com.gcloud.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

 

public class StreamConsumer extends Thread {
    private InputStream is;
    private File file;
    private String returnValue;

    public StreamConsumer(InputStream is) {
        this.is = is;
        returnValue = "";
    }

    public StreamConsumer(InputStream is, File file) {
        this(is);
        this.file = file;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void run() {
        BufferedOutputStream outStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedInputStream inStream = null;
        try {
            inStream = new BufferedInputStream(is);
            if (file != null) {
                fileOutputStream = new FileOutputStream(file);
				outStream = new BufferedOutputStream(fileOutputStream);
            }
            byte[] bytes = new byte[102400];
            int bytesRead;
            while ((bytesRead = inStream.read(bytes)) > 0) {
                returnValue += new String(bytes, 0, bytesRead);
                if (outStream != null) {
                    outStream.write(bytes, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (outStream != null) 
            {
                try 
                {
					outStream.close();
				} 
                catch (IOException e) 
				{
					e.printStackTrace();
				}
            }
            if (fileOutputStream != null) 
            {
                try 
                {
                    fileOutputStream.close();
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
            if (is != null) 
            {
                try 
                {
                    is.close();
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
            if (inStream != null) 
            {
                try 
                {
                    inStream.close();
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                }
            }
        }
    }
}