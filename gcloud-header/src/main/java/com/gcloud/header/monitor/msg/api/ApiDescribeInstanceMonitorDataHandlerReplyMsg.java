package com.gcloud.header.monitor.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.monitor.model.DescribeInstanceMonitorDataResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeInstanceMonitorDataHandlerReplyMsg extends ApiReplyMessage {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "云服务器监控数据")
	private DescribeInstanceMonitorDataResponse monitorData;

	public DescribeInstanceMonitorDataResponse getMonitorData() {
		return monitorData;
	}

	public void setMonitorData(DescribeInstanceMonitorDataResponse monitorData) {
		this.monitorData = monitorData;
	}
	
}