package com.gcloud.header.slb.model.standard;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardDescribeLoadBalancersResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "负载均衡器信�?")
	private List<StandardLoadBalancerModel> loadBalancer;

	public List<StandardLoadBalancerModel> getLoadBalancer() {
		return loadBalancer;
	}

	public void setLoadBalancer(List<StandardLoadBalancerModel> loadBalancer) {
		this.loadBalancer = loadBalancer;
	}
}