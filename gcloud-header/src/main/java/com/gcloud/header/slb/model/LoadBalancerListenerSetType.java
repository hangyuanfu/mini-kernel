package com.gcloud.header.slb.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LoadBalancerListenerSetType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "监听器ID")
	private String listenerId;
	@ApiModel(description = "监听器端�?")
	private Integer listenerPort;
	@ApiModel(description = "协议类型")
	private String listenerProtocol;
	@ApiModel(description = "状�??")
	private String status;
	@ApiModel(description = "服务器证书ID")
	private String serverCertificateId;
	@ApiModel(description = "后端服务器组ID")
	private String vserverGroupId;
	@ApiModel(description = "后端服务器组名称")
	private String vserverGroupName;
	
	public String getListenerId() {
		return listenerId;
	}
	public void setListenerId(String listenerId) {
		this.listenerId = listenerId;
	}
	public Integer getListenerPort() {
		return listenerPort;
	}
	public void setListenerPort(Integer listenerPort) {
		this.listenerPort = listenerPort;
	}
	public String getListenerProtocol() {
		return listenerProtocol;
	}
	public void setListenerProtocol(String listenerProtocol) {
		this.listenerProtocol = listenerProtocol;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getServerCertificateId() {
		return serverCertificateId;
	}
	public void setServerCertificateId(String serverCertificateId) {
		this.serverCertificateId = serverCertificateId;
	}
	public String getVserverGroupId() {
		return vserverGroupId;
	}
	public void setVserverGroupId(String vserverGroupId) {
		this.vserverGroupId = vserverGroupId;
	}
	public String getVserverGroupName() {
		return vserverGroupName;
	}
	public void setVserverGroupName(String vserverGroupName) {
		this.vserverGroupName = vserverGroupName;
	}
}