package com.gcloud.header.slb.msg.api.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiCreateLoadBalancerMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "名称，名字长度为[2-20]", require = true)
	@Length(min=2, max = 20, message = "0110101::名称长度为[2,20]")
	private String  loadBalancerName;
	@ApiModel(description = "子网ID", require = true)
	@NotBlank(message = "0110102::子网ID不能为空")
	private String   vSwitchId;

	@Override
	public Class replyClazz() {
		return StandardApiCreateLoadBalancerReplyMsg.class;
	}

	public String getLoadBalancerName() {
		return loadBalancerName;
	}

	public void setLoadBalancerName(String loadBalancerName) {
		this.loadBalancerName = loadBalancerName;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
}