package com.gcloud.header.slb.msg.api.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiSetLoadBalancerNameMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "负载均衡ID", require = true)
	@NotBlank(message = "0110202::ID不能为空")
	private String  loadBalancerId;
	@ApiModel(description = "名称,长度为[2,20]", require = true)
	@Length(min=2, max = 20, message = "0110203::名称长度为[2,20]")
	private String loadBalancerName;

	@Override
	public Class replyClazz() {
		return StandardApiSetLoadBalancerNameReplyMsg.class;
	}

	public String getLoadBalancerId() {
		return loadBalancerId;
	}

	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}

	public String getLoadBalancerName() {
		return loadBalancerName;
	}

	public void setLoadBalancerName(String loadBalancerName) {
		this.loadBalancerName = loadBalancerName;
	}
}