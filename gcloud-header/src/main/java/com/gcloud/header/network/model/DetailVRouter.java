package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailVRouter implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModel(description ="路由ID")
	private String id;
	@ApiModel(description = "路由名称")
	private String name;
	@ApiModel(description = "状�??, ACTIVE:�?�?;DOWN:失活;BUILD:已创�?;ERROR:错误;PENDING_CREATE:创建�?;PENDING_UPDATE:删除�?;PENDING_DELETE:删除�?;UNRECOGNIZED:未知;")
	private String status;
	@ApiModel(description = "中文状�??")
	private String cnStatus;
	@ApiModel(description = "交换机信息集�?")
	private List<VSwitchSetType> subnets;
	@ApiModel(description = "网络ID")
	private String externalNetworkId;
	@ApiModel(description="网络名称")
	private String externalNetworkName;
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	@ApiModel(description="用户ID")
	private String userId;
	@ApiModel(description="用户名称")
	private String userName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public List<VSwitchSetType> getSubnets() {
		return subnets;
	}
	public void setSubnets(List<VSwitchSetType> subnets) {
		this.subnets = subnets;
	}
	public String getExternalNetworkId() {
		return externalNetworkId;
	}
	public void setExternalNetworkId(String externalNetworkId) {
		this.externalNetworkId = externalNetworkId;
	}
	public String getExternalNetworkName() {
		return externalNetworkName;
	}
	public void setExternalNetworkName(String externalNetworkName) {
		this.externalNetworkName = externalNetworkName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}