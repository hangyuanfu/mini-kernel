package com.gcloud.header.network.model;

import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DetailSubnet implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModel(description="子网ID")
    private String id;
    @ApiModel(description="子网名称")
    private String name;
    @ApiModel(description="子网网段")
    @TableField("cidr")
    private String cidrBlock;
    @ApiModel(description = "网关IP")
    private String gatewayIp;
    @ApiModel(description = "dns服务")
    private String dnsServers;
    @ApiModel(description = "DHCP分配�?")
    private List<AllocationPool> allocationPools;
    @ApiModel(description = "是否启用DHCP，true：启用；false：禁�?")
    private Boolean enableDhcp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCidrBlock() {
        return cidrBlock;
    }

    public void setCidrBlock(String cidrBlock) {
        this.cidrBlock = cidrBlock;
    }

    public String getGatewayIp() {
        return gatewayIp;
    }

    public void setGatewayIp(String gatewayIp) {
        this.gatewayIp = gatewayIp;
    }

    public String getDnsServers() {
        return dnsServers;
    }

    public void setDnsServers(String dnsServers) {
        this.dnsServers = dnsServers;
    }

    public List<AllocationPool> getAllocationPools() {
        return allocationPools;
    }

    public void setAllocationPools(List<AllocationPool> allocationPools) {
        this.allocationPools = allocationPools;
    }

    public Boolean getEnableDhcp() {
        return enableDhcp;
    }

    public void setEnableDhcp(Boolean enableDhcp) {
        this.enableDhcp = enableDhcp;
    }
}