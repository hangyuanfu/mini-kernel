package com.gcloud.header.network.msg.api;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesMsg;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeNetworkInterfacesMsg extends StandardDescribeNetworkInterfacesMsg {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "端口的拥有�??")
	private List<String> deviceOwners;

	@ApiModel(description = "是否包含没有拥有�?,true:�?,false:�?")
	private Boolean includeOwnerless;

	@ApiModel(description = "IP")
	private String primaryIpAddress;
	
	@ApiModel(description = "是否可用,true:可用,false:已用")
	private Boolean avail;

	@ApiModel(description = "是否只显示ENI设备,true:�?,false:�?")
	private Boolean eni;
	
	@ApiModel(description = "是否绑定了浮动IP, true:�?, false:�?")
	private Boolean isFixed;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return DescribeNetworkInterfacesReplyMsg.class;
	}

	public Boolean getAvail() {
		return avail;
	}

	public void setAvail(Boolean avail) {
		this.avail = avail;
	}

	public List<String> getDeviceOwners() {
		return deviceOwners;
	}

	public void setDeviceOwners(List<String> deviceOwners) {
		this.deviceOwners = deviceOwners;
	}

	public Boolean getIncludeOwnerless() {
		return includeOwnerless;
	}

	public void setIncludeOwnerless(Boolean includeOwnerless) {
		this.includeOwnerless = includeOwnerless;
	}

	@Override
	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}

	@Override
	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}

	public Boolean getEni() {
		return eni;
	}

	public void setEni(Boolean eni) {
		this.eni = eni;
	}

	public Boolean getIsFixed() {
		return isFixed;
	}

	public void setIsFixed(Boolean isFixed) {
		this.isFixed = isFixed;
	}
}