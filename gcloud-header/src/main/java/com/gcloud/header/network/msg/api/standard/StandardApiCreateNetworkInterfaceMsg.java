package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.common.RegExp;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiCreateNetworkInterfaceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "子网ID", require = true)
	@NotBlank(message = "0080101::子网ID不能为空")
	private String vswitchId;
	@ApiModel(description = "IP地址")
	@Pattern(regexp = RegExp.IPV4, message = "0080105::请输入正确的IP")
	private String primaryIpAddress;
	@ApiModel(description = "安全组ID", require = true)
	@NotBlank(message = "0080102::安全组ID不能为空")
	private String securityGroupId;
	@ApiModel(description = "网卡名称")
    @Length(max = 255, message = "0080106::名称长度不能大于255")
	private String networkInterfaceName;
	@ApiModel(description = "描述")
    @Length(max = 255, message = "0080107::描述长度不能大于255")
	private String description;

	@Override
	public Class replyClazz() {
		return StandardApiCreateNetworkInterfaceReplyMsg.class;
	}

	public String getVswitchId() {
		return vswitchId;
	}

	public void setVswitchId(String vswitchId) {
		this.vswitchId = vswitchId;
	}

	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}

	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}