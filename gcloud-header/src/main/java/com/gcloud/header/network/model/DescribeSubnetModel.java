package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeSubnetModel implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="子网ID")
	private String id;
	@ApiModel(description="子网名称")
	private String name;
	@ApiModel(description="子网网段")
	@TableField("cidr")
	private String cidrBlock;
	@ApiModel(description="创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	@ApiModel(description="部门ID")
	private String departmentId;
	@ApiModel(description="网络ID")
	private String networkId;
	@ApiModel(description="租户ID�?")
	private String tenantId;
	@ApiModel(description="租户名称")
	private String tenantName;
	@ApiModel(description="用户ID")
	private String userId;
	@ApiModel(description="用户名称")
	private String userName;
	@ApiModel(description="路由ID")
	private String vrouterId;
	@ApiModel(description="可用区ID")
	private String zoneId;
	@ApiModel(description="可用�?")
	private String zoneName;
	@ApiModel(description = "网关IP")
	private String gatewayIp;
	@ApiModel(description = "dns服务")
	private String dnsServers;
	@ApiModel(description = "是否启用DHCP")
	private Boolean enableDhcp;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getVrouterId() {
		return vrouterId;
	}
	public void setVrouterId(String vrouterId) {
		this.vrouterId = vrouterId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getGatewayIp() {
		return gatewayIp;
	}
	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}
	public String getDnsServers() {
		return dnsServers;
	}
	public void setDnsServers(String dnsServers) {
		this.dnsServers = dnsServers;
	}
	public Boolean getEnableDhcp() {
		return enableDhcp;
	}
	public void setEnableDhcp(Boolean enableDhcp) {
		this.enableDhcp = enableDhcp;
	}
}