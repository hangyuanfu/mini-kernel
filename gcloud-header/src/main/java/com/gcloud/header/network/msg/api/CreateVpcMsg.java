package com.gcloud.header.network.msg.api;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiCreateMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateVpcMsg extends ApiCreateMessage {
	
	@ApiModel(description="专有网络ID", require=true)
	@NotBlank(message = "0100101::专有网络名称不能为空")
	private String vpcName;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return CreateVpcReplyMsg.class;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	

}