package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDescribeDisksMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeDisksMsg extends StandardApiDescribeDisksMsg {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiDescribeDisksReplyMsg.class;
    }
    
    @ApiModel(description = "筛�?�出创建了快照的磁盘, 默认为false:不进行筛�?")
    private boolean hasSnapshot = false;

	public boolean isHasSnapshot() {
		return hasSnapshot;
	}

	public void setHasSnapshot(boolean hasSnapshot) {
		this.hasSnapshot = hasSnapshot;
	}
}