package com.gcloud.header.storage.msg.api.pool;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiPoolsStatisticsReplyMsg extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "存储是数�?")
	private int poolNum;
	@ApiModel(description = "存储池�?�容�?")
	private long poolTotalSize;
	@ApiModel(description = "存储池已使用容量")
	private long poolUsedSize;
	@ApiModel(description = "存储池剩余容�?")
	private long poolAvailSize;
	public int getPoolNum() {
		return poolNum;
	}
	public void setPoolNum(int poolNum) {
		this.poolNum = poolNum;
	}
	public long getPoolTotalSize() {
		return poolTotalSize;
	}
	public void setPoolTotalSize(long poolTotalSize) {
		this.poolTotalSize = poolTotalSize;
	}
	public long getPoolUsedSize() {
		return poolUsedSize;
	}
	public void setPoolUsedSize(long poolUsedSize) {
		this.poolUsedSize = poolUsedSize;
	}
	public long getPoolAvailSize() {
		return poolAvailSize;
	}
	public void setPoolAvailSize(long poolAvailSize) {
		this.poolAvailSize = poolAvailSize;
	}
	
}