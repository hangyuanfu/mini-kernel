package com.gcloud.header.storage.msg.api.pool;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiCreateDiskCategoryMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiCreateDiskCategoryReplyMsg.class;
    }
    
    @ApiModel(description = "磁盘类型名称", require = true)
    @Length(min = 1, max = 255, message = StorageErrorCodes.INPUT_CATEGORY_NAME_ERROR)
    @NotBlank(message = "::磁盘类型名称不能为空")
    private String name;
    
    @ApiModel(description = "磁盘�?小大�?", require = false)
    @Min(value = 1, message = StorageErrorCodes.INPUT_DISK_SIZE_ERROR)
    private Integer minSize;
    
    @ApiModel(description = "磁盘�?大大�?", require = false)
    @Min(value = 1, message = StorageErrorCodes.INPUT_DISK_SIZE_ERROR)
    private Integer maxSize;
    
    @ApiModel(description = "存储类型, local:本地存储, distributed:分布式存�?, central:集中存储")
    @NotBlank(message = "::存储类型不能为空")
    private String storageType;
    
    @ApiModel(description = "可否使用，默认为可用", require = false)
    private boolean enabled = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public void setMinSize(Integer minSize) {
        this.minSize = minSize;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}