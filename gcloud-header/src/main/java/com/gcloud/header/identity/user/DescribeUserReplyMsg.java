package com.gcloud.header.identity.user;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeUserResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeUserReplyMsg extends PageReplyMessage<UserModel>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="用户列表")
	private DescribeUserResponse users;
	
	@Override
	public void setList(List<UserModel> list) {
		users = new DescribeUserResponse();
		users.setUser(list);
	}

	public DescribeUserResponse getUsers() {
		return users;
	}

	public void setUsers(DescribeUserResponse users) {
		this.users = users;
	}
	
}