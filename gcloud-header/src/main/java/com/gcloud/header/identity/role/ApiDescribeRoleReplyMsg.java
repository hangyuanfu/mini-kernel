package com.gcloud.header.identity.role;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeRoleResponse;
import com.gcloud.header.identity.model.RoleModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeRoleReplyMsg extends PageReplyMessage<RoleModel>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="角色列表")
	private DescribeRoleResponse roles;
	
	@Override
	public void setList(List<RoleModel> list) {
		roles = new DescribeRoleResponse();
		roles.setRole(list);
	}

	public DescribeRoleResponse getRoles() {
		return roles;
	}

	public void setRoles(DescribeRoleResponse roles) {
		this.roles = roles;
	}

}