package com.gcloud.header.identity.api;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.role.ApiGetFunctionRightReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class GetApiMsg extends ApiMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="功能路径")
	@NotBlank(message="2040101::功能路径不能为空")
	private String path;

	@Override
	public Class replyClazz() {
		return GetApiReplyMsg.class;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}