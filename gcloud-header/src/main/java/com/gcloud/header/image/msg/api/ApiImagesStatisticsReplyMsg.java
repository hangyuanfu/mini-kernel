package com.gcloud.header.image.msg.api;

import java.util.List;

import com.gcloud.header.ListReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.image.model.ImageStatisticsItem;
import com.gcloud.header.image.model.ImageStatisticsResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiImagesStatisticsReplyMsg extends ListReplyMessage<ImageStatisticsItem>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "统计信息")
	ImageStatisticsResponse statisticsItems;
	@ApiModel(description = "总数�?")
	int allNum;
	
	@Override
	public void setList(List<ImageStatisticsItem> list) {
		statisticsItems = new ImageStatisticsResponse();
		statisticsItems.setStatisticsItem(list);
	}
	public ImageStatisticsResponse getStatisticsItems() {
		return statisticsItems;
	}
	public void setStatisticsItems(ImageStatisticsResponse statisticsItems) {
		this.statisticsItems = statisticsItems;
	}
	public int getAllNum() {
		return allNum;
	}
	public void setAllNum(int allNum) {
		this.allNum = allNum;
	}
	
}