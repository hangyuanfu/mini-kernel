package com.gcloud.header.image.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum ImageStatus {

    UNRECOGNIZED("未知"),
    QUEUED("队列�?"),
    SAVING("保存�?"),
    ACTIVE("可用"),
    DEACTIVATED("不可�?"),
    KILLED("终止"),
    DELETED("已删�?"),
    PENDING_DELETE("删除�?");

    private String cnName;

    ImageStatus(String cnName) {
        this.cnName = cnName;
    }

    public String value() {
        return name().toLowerCase();
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String value) {
    	ImageStatus enu =  Arrays.stream(ImageStatus.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
		return enu != null ? enu.getCnName() : null;
	}
}