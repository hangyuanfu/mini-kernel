package com.gcloud.header.image.model.iso;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class IsoType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description="映像ID")
    @TableField("id")
    private String isoId;
	@ApiModel(description="映像名称")
    @TableField("name")
    private String isoName;
	@ApiModel(description="备注")
    private String description;
	@ApiModel(description="大小")
    private Long size;
	@ApiModel(description="状�?�，saving：保存中，active：可�?")
    private String status;
	@ApiModel(description="中文状�??")
    private String cnStatus;
	@ApiModel(description="创建时间")
    @TableField("created_at")
    @JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
    private Date creationTime;
	@ApiModel(description="架构, x86_64、i386�? i686")
    private String architecture;
	@ApiModel(description="映像拥有者别�?")
    private String isoOwnerAlias;
	@ApiModel(description="操作系统类型，linux、windows")
    private String osType;
	@ApiModel(description="是否禁用，true:禁用, false:可用")
    private Boolean disable;
	@ApiModel(description="映像类型，system:系统光盘, other:其他")
    private String isoType;//system\other
	@ApiModel(description="中文映像类型")
    private String cnIsoType;
	@ApiModel(description="操作系统版本")
    private String osVersion;//如CentOS 7.0 Minimal-1503�?'
	@ApiModel(description="映像格式，iso")
    private String format;
	public String getCnIsoType() {
		return cnIsoType;
	}
	public void setCnIsoType(String cnIsoType) {
		this.cnIsoType = cnIsoType;
	}
	public String getIsoId() {
		return isoId;
	}
	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}
	public String getIsoName() {
		return isoName;
	}
	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getArchitecture() {
		return architecture;
	}
	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}
	public String getIsoOwnerAlias() {
		return isoOwnerAlias;
	}
	public void setIsoOwnerAlias(String isoOwnerAlias) {
		this.isoOwnerAlias = isoOwnerAlias;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
	public String getIsoType() {
		return isoType;
	}
	public void setIsoType(String isoType) {
		this.isoType = isoType;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	
}