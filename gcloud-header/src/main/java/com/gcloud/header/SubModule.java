package com.gcloud.header;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum SubModule {
	NONE(null),
	VM("虚拟机实例管�?"),
	DISK("磁盘管理"),
	IMAGE("镜像管理"),
	SNAPSHOT("快照管理"),
	VPC("专有网络管理"),
	VROUTER("路由器管�?"),
	VSWITCH("交换机管�?"),
	SECURITYGROUP("安全组管�?"),
	NETWORKINTERFACE("网卡管理"),
	EIPADDRSS("公网弹�?�IP管理"),
	SECURITYCLUSTER("安全集群"),
	NETWORK("网络管理"),
	SUBNET("子网管理"),
	NODE("节点管理");
	String cnName;
	private SubModule(String cnName) {
		// TODO Auto-generated constructor stub
		this.cnName=cnName;
	}
	public String getCnName(){
		return cnName;
	}
}