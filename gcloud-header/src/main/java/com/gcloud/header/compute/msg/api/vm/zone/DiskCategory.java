package com.gcloud.header.compute.msg.api.vm.zone;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DiskCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ApiModel(description = "磁盘类型ID")
    private String diskTypeId;
    @ApiModel(description = "磁盘类型名称")
    private String diskTypeName;
    @ApiModel(description = "磁盘类型中文名称")
    private String diskTypeCnName;
    @ApiModel(description = "磁盘�?小容�?, 单位:MB")
    private Integer min;
    @ApiModel(description = "磁盘�?大容�?, 单位:MB")
    private Integer max;

    public String getDiskTypeId() {
        return diskTypeId;
    }

    public void setDiskTypeId(String diskTypeId) {
        this.diskTypeId = diskTypeId;
    }

    public String getDiskTypeName() {
        return diskTypeName;
    }

    public void setDiskTypeName(String diskTypeName) {
        this.diskTypeName = diskTypeName;
    }

    public String getDiskTypeCnName() {
        return diskTypeCnName;
    }

    public void setDiskTypeCnName(String diskTypeCnName) {
        this.diskTypeCnName = diskTypeCnName;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

}