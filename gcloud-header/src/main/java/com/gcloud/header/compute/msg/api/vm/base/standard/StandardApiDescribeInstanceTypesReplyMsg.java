package com.gcloud.header.compute.msg.api.vm.base.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.standard.StandardDescribeInstanceTypesResponse;
import com.gcloud.header.compute.msg.api.model.standard.StandardInstanceTypeItemType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeInstanceTypesReplyMsg extends PageReplyMessage<StandardInstanceTypeItemType>{

	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "实例类型列表")
	private StandardDescribeInstanceTypesResponse instanceTypes;

	@Override
	public void setList(List<StandardInstanceTypeItemType> list) {
		instanceTypes = new StandardDescribeInstanceTypesResponse();
		instanceTypes.setInstanceType(list);
	}

	public StandardDescribeInstanceTypesResponse getInstanceTypes() {
		return instanceTypes;
	}

	public void setInstanceTypes(StandardDescribeInstanceTypesResponse instanceTypes) {
		this.instanceTypes = instanceTypes;
	}
}