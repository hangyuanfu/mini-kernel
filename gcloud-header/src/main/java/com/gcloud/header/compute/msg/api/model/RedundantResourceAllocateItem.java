package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class RedundantResourceAllocateItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "实例类型")
    private String instanceType;
	@ApiModel(description = "配置")
    private String config;
	@ApiModel(description = "�?大可分配")
	private int maxAllocate;
	@ApiModel(description = "实际可分�?")
	private int actualAllocate;
	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public int getMaxAllocate() {
		return maxAllocate;
	}
	public void setMaxAllocate(int maxAllocate) {
		this.maxAllocate = maxAllocate;
	}
	public int getActualAllocate() {
		return actualAllocate;
	}
	public void setActualAllocate(int actualAllocate) {
		this.actualAllocate = actualAllocate;
	}
	
}