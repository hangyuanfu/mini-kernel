package com.gcloud.header.compute.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum Device {

	VDA("vda", "/dev/vda"),
	VDB("vdb", "/dev/vdb"),
	VDZ("vdz", "/dev/vdz"),
	FDA("fda", "/dev/fda"),
	HDC("hdc", "/dev/hdc");
	
	private String value;
	private String mountPath;

	Device(String value, String mountPath) {
		this.value = value;
		this.mountPath = mountPath;
	}

	public String getValue() {
		return value;
	}

	public String getMountPath() {
		return mountPath;
	}
}