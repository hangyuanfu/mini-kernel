package com.gcloud.header.compute.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum FsfreezeType {
	
	THAWED("thawed"), FROZEN("frozen");
	
	private String value;
	FsfreezeType(String name){
		this.value = name;
	}

	public String getValue() {
		return value;
	}

	public static FsfreezeType getByValue(String value) {
		for (FsfreezeType ft : FsfreezeType.values()) {
			if (ft.getValue().equals(value)) {
				return ft;
			}
		}
		return null;
	}
	
}