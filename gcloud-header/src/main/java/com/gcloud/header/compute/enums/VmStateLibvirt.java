/*
 * @Date 2015-5-15
 * 
 * @Author chenyu1@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * 
 */
package com.gcloud.header.compute.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum VmStateLibvirt {
	NOSTATE("Extant"), RUNNING("running"), BLOCKED("Extant"), PAUSED("paused"), SHUTDOWN(
			"Shutdown"), SHUTOFF("shut off"), CRASHED("StateException"), INSHUTDOWN("in shutdown");
	private String value;

	VmStateLibvirt(String name) {
		this.value = name;
	}

	public String getValue() {
		return value;
	}

}