package com.gcloud.header.compute.msg.api.vm.zone;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiSetZoneComputeNodesMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;


    @Override
    public Class replyClazz() {
        return ApiSetZoneComputeNodesReplyMsg.class;
    }

    @ApiModel(description = "可用区ID", require = true)
    @NotBlank(message = "0200101::可用区ID不能为空")
    private String zoneId;
    
    @ApiModel(description = "节点名称列表")
    private List<String> hostNames;
    
    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

	public List<String> getHostNames() {
		return hostNames;
	}

	public void setHostNames(List<String> hostNames) {
		this.hostNames = hostNames;
	}

}